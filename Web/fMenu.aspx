﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fMenu.aspx.vb" Inherits="YPF_Mobile.fMenu" %>
    <asp:Content ID="Content2" ContentPlaceHolderID="Contenido" runat="server">
        <section id="content">
            <section class="vbox">
                <section class="scrollable wrapper">
                    <div class="col-xs-12 no-padder">
                        <section class="pan el  animated fadeInUpBig hidden">
                            <div class="panel-body  bg-primary dker ">
                                <div class="clearfix m-b col-sm-12 no-padder">
                                    <div class="col-sm-1 no-padder"> <span class="pull-left">
                                            <i class="fa fa-road text-warning fa-3x m-t-xs"></i> 
			                            </span> </div>
                                    <div class="clear col-sm-10 no-padder">
                                        <h1 class="m-b-none m-t-sm"><strong>YPF Mobile</strong></h1> <small class="block h3 text-muted">Bienvenido</small>
                                        <h5>
                                            <label class="badge bg-primary dk"><asp:Literal ID="ltl_usuario" runat="server"></asp:Literal></label>
                                            

                                        </h5> </div>
                                </div>
                            </div>
                            <footer class="panel-footer pos-rlt"> <span class="arrow top"></span>
                                <div class="pull-out">
                                    <p class="h6 m-l m-t"> Si necesitás ayuda podés contactarnos por estos medios<br /> Email: <a href="mailto:mesadeayuda@digip.com.ar">Mesadeayuda@digip.com.ar</a><br /> Telefono: 011.5199.5725 </p>
                                    <h4>
                                        <asp:Literal ID="lt_usuario_agente2" runat="server"></asp:Literal>
                                    </h4> </div>
                            </footer>
                        </section>
                        <div class=" animated fadeInUpBig ">
                            <div class="col-md -7 no-padder no-borders">
                                <section class="pan el bg-primary dker no-borders">
                                    <div class="panel-heading dker h4 no-borders text-center"> Bienvenido a YPF Mobile </div>
                                    <div class="panel-body no-borders">
                                        <a href="/app/fprecios.aspx">
                                            <article class="media col-xs-5 btn btn-primary m-b m-t-none">
                                                <p> <i class="fa fa-shopping-cart fa-4x   "></i></p>
                                                <p class="h5 font-thin">Precargar<br />combustible</p>
                                            </article>
                                            <div class="col-xs-2 m-b"></div>
                                        </a>
                                        <a href="/app/fmapa.aspx">
                                            <article class="media col-xs-5 btn btn-primary m-b m-t-none">
                                                <p> <i class="fa fa-map-marker fa-4x   "></i></p>
                                                <p class="h5 font-thin">Estaciones <br />mas cercanas</p>
                                            </article>
                                        </a>
                                        <a href="/app/fcomprobantespendientes.aspx">
                                            <article class="media col-xs-5 btn btn-primary m-b m-t-none">
                                                <p> <i class="fa fa-files-o fa-4x   "></i></p>
                                                <p class="h5 font-thin">Cargas<br />pendientes</p>
                                            </article>
                                        </a>
                                        <div class="col-xs-2 "></div>
                                        <a href="/app/fcomprobantes.aspx">
                                            <article class="media col-xs-5 btn btn-primary m-b m-t-none">
                                                <p> <i class="fa fa-clock-o fa-4x   "></i></p>
                                                <p class="h5 font-thin">Historial<br />de consumos</p>
                                            </article>
                                        </a> <br />
                                        <a href="http://www.serviclub.com.ar">
                                            <article class="media col-xs-12 col-sm-5 btn btn-primary m-b m-t-none" style="background-image: url('/serviclub.jpg'); background-size: cover; background-position:center">
                                                <p> <i class="fa fa-files-o fa-4x" style="color:rgba(255, 255, 255, 0)"></i></p>
                                                <p class="h5 font-thin" style="color:rgba(255, 255, 255, 0)">.<br />,</p>
                                            </article>
                                        </a>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </asp:Content>