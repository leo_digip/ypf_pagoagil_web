﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="fRegistracion.aspx.vb" Inherits="YPF_Mobile.fRegistracion" %>

<!DOCTYPE html>
<html lang="es">
<head  id="Head1">
  <meta charset="utf-8" />
  <title>YPF Mobile</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/bootstrap.css" ) %>" type="text/css"   />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/animate.css" ) %>" type="text/css"    />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font-awesome.min.css"  ) %>" type="text/css"   />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font.css" ) %>" type="text/css" />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/plugin.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/app.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datatables/datatables.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/select2/select2.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datepicker/datepicker.css" ) %>" type="text/css"  />
  <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/slider/slider.css" ) %>" type="text/css"  />


    
  
   <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/jquery.min.js" )%>"></script>
  <!-- Bootstrap -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/bootstrap.js" )%>"></script>


  <!--[if lt IE 9]>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/respond.min.js" )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/html5.js"  )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/fix.js"  )%>"></script>
  <![endif]-->
  
 <SCRIPT LANGUAGE="JavaScript">
     function popup(URL, ancho, alto, id) {
         day = new Date();
         eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=" + ancho + ", height =" + alto + ",left = 625,top = 210');");
     }</script>

  
  
 <style>
html, body{height:100%; margin:0;padding:0}
 
.container-fluid{
  height:100%;
  display:table;
  width: 100%;
  padding: 0;
}
 
.row-fluid {height: 100%; display:table-cell; vertical-align: middle;}
 
.centering {
  float:none;
  margin:0 auto;
}


</style>

</head>

<body style="background-image: url('fondo_inicio4.png'); " >



<div class="container-fluid  ">
    <div class="row-fluid">
        <div class="centering" >
  

   
            
<form id="padre" role="form"     runat="server">

 <div class="animated fadeInDownBig wrapper-lg "  id="pnl_login" runat="server">  

         <section class="">
                      <header class="text-center text-white" >
                        <h1>Bienvenido a YPF Mobile</h1>
                          <h4>Registre sus datos personales para comenzar</h4>
                          <br />
                          <br />

                      </header>
             </section>



                        <!-- nuevo -->
					<div class="padder-v bg-primary dker no-borders" id="div_nuevo">
                        <br />
						<section class="panel-body">
							<div class ="bs-example form-horizontal" >
                                <div class ="form-group">
                                    <label class ="col-sm-1">Nº Tarjeta</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_tarjeta" CssClass="form-control" required="true" type="number"  runat="server"></asp:TextBox>
									</div>
                                    
                                    <label class ="col-sm-1">PIN</label>   
									<div class ="col-sm-2">
                                        <asp:TextBox ID="txt_pin" runat="server" required="true" type="number" ></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Codigo verificador</label>   
									<div class ="col-sm-2">
                                        <asp:TextBox ID="txt_verificador" runat="server" required="true" type="number"  ></asp:TextBox>
									</div>
								</div>
                                 
                                <div class ="form-group">
									<label class ="col-sm-1">Apellido</label>   
									<div class ="col-sm-2">
										<asp:TextBox ID="txt_apellido" CssClass="form-control" required="true"  runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Nombre</label>   
									<div class ="col-sm-2">
										<asp:TextBox ID="txt_nombre" CssClass="form-control" required="true"  runat="server"></asp:TextBox>
									</div>
									<label class ="col-sm-1">DNI</label>   
									<div class ="col-sm-2">
										<asp:TextBox ID="txt_doc" CssClass="form-control" required="true"  type="number" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Fecha nacimiento</label>   
									<div class ="col-sm-2">
                                        <asp:TextBox ID="dt_fechanac" runat="server" required="true"   type="date"></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Calle</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_calle" CssClass="form-control" required="true"  runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Altura</label>   
									<div class ="col-sm-1">
										<asp:TextBox ID="txt_altura" CssClass="form-control" required="true" type="number" runat="server"></asp:TextBox>
									</div>
                                </div>
                                <div class ="form-group">
                                    <label class ="col-sm-1">Localidad</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_localidad" CssClass="form-control" required="true"  runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Provincia</label>   
									<div class ="col-sm-5">
                                        <asp:DropDownList ID="cbo_provincia" CssClass="form-control"  runat="server"></asp:DropDownList>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Telefono</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_cel" CssClass="form-control" required="true"  type="number" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Email</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_email" CssClass="form-control" type="email" required="true" runat="server"></asp:TextBox>
									</div>
								</div>


                                <div class ="form-group">
									<label class ="col-sm-1">Contraseña web</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_password" CssClass="form-control"  required="true" runat="server"></asp:TextBox>
									</div>
									<label class ="col-sm-1">Reitere contraseña</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_password2" CssClass="form-control" required="true"  runat="server"></asp:TextBox>
									</div>
								</div>

							</div>
                            <asp:Button ID="btn_guardar" runat="server" class="btn btn-lg btn-primary dker pull-right" text="Guardar y continuar"  />
              
						</section>

					</div>


</div>
   
    </form>
    </div>
        </div>
    </div>
   
    </body>
    </html>
