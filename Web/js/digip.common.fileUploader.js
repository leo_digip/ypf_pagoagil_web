﻿var common = namespace('digip.common');



common.fileUploader = function(e, params){

    var indoor_map;
    
    
    if($('.selectedFile').html() != ''){
        $('.indoor_bkg').attr('value', $('.selectedFile').html());
        if(indoor_map){
            indoor_map.clean();
        }
        indoor_map = new digip.common.indoor('#indoor', {
            url: $('#preURL').attr('value') + $('.selectedFile').html()
        });
    }
    
    $(e).fileupload(
    {
        dataType: 'json',
        url: $('#preURL').attr('value') +  'app/AjaxFileHandler.ashx',
        add: function (e, data) {
            var re = /^.+\.((jpg)|(png))$/i,
            valid = true;

            for (var i = 0, l = data.files.length; i < l; i++) {
                if (!re.test(data.files[i].name)) {
                    valid = false;
                }
            }

            if (valid) {
                data.submit();
            }
            else {
                alert('Selected file is not a valid image type (JPG or PNG).');
            }
        },
        start: function (e, data) {
        },
        progress: function (e, data) {
        },
        done: function (e, data) {
            $('.selectedFile').html(data.result.filename);
            $('.indoor_bkg').attr('value', data.result.filename);
            if(indoor_map){
                indoor_map.clean();
            }
            indoor_map = new digip.common.indoor('#indoor', {
                url: $('#preURL').attr('value') + data.result.filename
            });

        },
        fail: function (e, data) {


            alert('Image upload failed. Please consider validating with your application administrator that your Web.config settings in your web server has the maxRequestLength and requestFiltering properties properly set.')
        }
    });

};
$(document).ready(function(){
    $('[data-fileUploader="true"').each(function(){
        console.log('file uploader initialized...');
        new digip.common.fileUploader(this, {});
    });

});