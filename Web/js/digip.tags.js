﻿
var tags = namespace('digip.tags');


tags.cargarAreas = function() {
    var dfd = new jQuery.Deferred();
    $.ajax({
        type: "post",

        url: $('#preURL').attr('value') +  "app/handler.asmx/CargarAreas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_area');
            var options = '';
            options += '<option value="-1">(Todas)</option>';
            for (var i = 0, l = data.length; i < l; i++) {
                options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });


            //carga las ubicaciones del area seleccionada:
            var areaId = $('#dd_area').val();
            //console.log("selected area: " + areaId);
            if (areaId) {
                $.when(tags.cargarUbicaciones(areaId)).done(function () {
                    dfd.resolve();
                });
            }
            else {
                dfd.resolve();
            }


            $select.on('change', function () {
                tags.cargarUbicaciones(this.value);
            });
        },
        error: function (xhr, status, error) {
            alert(error);
            dfd.resolve();
        }
    });

    return dfd.promise();

}


tags.cargarUbicaciones = function(areaId) {
    return $.ajax({
        type: "post",
        //url: "<%  = Page.ResolveClientUrl("~/app/ads/handler.asmx/Stats_HeatViews",
        url: $('#preURL').attr('value') +  "app/handler.asmx/CargarUbicaciones",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: '{"areaId":"' + areaId + '"}',
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_ubicacion');
            var options = '';
            options += '<option value="-1">(Todas)</option>';
            for (var i = 0, l = data.length; i < l; i++) {
                options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });



        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });

}


tags.cargarTiposDeSensor = function() {
    return $.ajax({
        type: "post",

        url: $('#preURL').attr('value') +  "app/handler.asmx/CargarTiposDeSensor",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_tipo');
            var options = '';
            options += '<option value="-1">(Todos)</option>';
            for (var i = 0, l = data.length; i < l; i++) {
                options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });

}

tags.cargarTiposDeAlerta = function (tipo) {
    return $.ajax({
        type: "post",

        url: $('#preURL').attr('value') +  "app/handler.asmx/CargarTiposDeAlerta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_tipoAlerta');
            var options = '';
            options += '<option value="-1">(Todas)</option>';
            for (var i = 0, l = data.length; i < l; i++) {
                if (tipo != null && data[i].value == tipo) {
                    options += '<option selected value="' + data[i].value + '">' + data[i].text + '</option>';
                }
                else {
                    options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
                }
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });

}

tags.cargarTiposMov = function () {
    return $.ajax({
        type: "post",

        url: $('#preURL').attr('value') + "app/handler.asmx/CargarTiposMov",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_tipoMov');
            var options = '';
            options += '<option value="-1">(Todas)</option>';
            for (var i = 0, l = data.length; i < l; i++) {
                
                    options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
                
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });

}

tags.cargarSensores = function(ocultarSelectorTodos) {
    return $.ajax({
        type: "post",

        url: $('#preURL').attr('value') +  "app/handler.asmx/CargarSensores",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            var $select = $('#dd_sensor');
            var options = '';
            if (!ocultarSelectorTodos) {
                options += '<option value="-1">(Todos)</option>';
            }
            for (var i = 0, l = data.length; i < l; i++) {
                options += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $select.html(options);
            $select.chosen({ no_results_text: "Sin resultados." });
            
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });
}

tags.cargarTags = function () {
    var dfd = new jQuery.Deferred();
    $.ajax({
        type: "post",
        url: $('#preURL').attr('value') + "app/handler.asmx/CargarTags",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            dfd.resolve(data);

        },
        error: function (xhr, status, error) {
            alert(error);
            dfd.resolve([]);
        }
    });
    return dfd.promise();
}

tags.cargarContactos = function () {
    var dfd = new jQuery.Deferred();
    $.ajax({
        type: "post",
        url: $('#preURL').attr('value') + "app/handler.asmx/CargarContactos",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var data = result['d'];
            dfd.resolve(data);

        },
        error: function (xhr, status, error) {
            alert(error);
            dfd.resolve([]);
        }
    });
    return dfd.promise();
}
