﻿var tags = namespace('digip.tags');
var iniciadoAlertas = false;
var iniciadoConsola = false;
tags.iniciarSocketAlertas = function (callback) {
    if(iniciadoAlertas){
        return false;
    }
    iniciadoAlertas = true;
    var alertasMostradas = []
    var preURL = $('#preURL').attr('value');
    var ultimoPedido = new Date();
    var mult = 1;

    (function poll() {
        setTimeout(function () {
            //console.log('llamando...')
            mult = 1;
            $.ajax({
                type: "post",
                url: preURL + "app/handler.asmx/CargarCantidadDeAlertasNuevas",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    ultimoPedido: ultimoPedido
                }),
                success: function (result) {
                    //console.log('success...');
                    ultimoPedido = new Date();
                    try {
                        var data = result['d']
                        
                        if (data.length > 0)
                        {
                            for (var i = 0; i < data.length; i++) {
                                if ($.inArray(data[i].idAlerta, alertasMostradas) == -1){
                                    alertasMostradas.push(data[i].idAlerta);
                                
                                    $.growl({
                                        icon: 'alertaIcono fa  fa-bell fa-2x animated swing',
                                        title: '<span class="alertaTitulo"><strong>'+ data[i].descripcion+'</strong><span> ',
                                        message:'<span class="alertaMensaje">'+ data[i].sensor +'<span> ',
                                        url: preURL + 'app/stats/lista_alertas.aspx?noleidas=1&tipo=' + data[i].tipoDeAlerta
                                    }, {
                                        type: 'danger',
                                        delay: 30000,
                                        animate: {
                                            enter: 'animated tada',
                                            exit: 'animated hinge'
                                        },
                                        template: '<div data-growl="container" class="alert" role="alert" title="Click para ir a Alertas">\
		                                            <button type="button" class="close" data-growl="dismiss">\
			                                            <span aria-hidden="true">×</span>\
			                                            <span class="sr-only">Close</span>\
		                                            </button>\
		                                            <span data-growl="icon" style=""></span>\
		                                            <span data-growl="title"></span>\
		                                            <span data-growl="message"></span>\
		                                            <a href="#" data-growl="url"></a>\
	                                            </div>'

                                    });
                                }
                            }
                       } 
                    }
                    finally {
                        poll();
                    }
                }, dataType: "json"
            });
        }, 30000 * mult);
    })();
}

tags.iniciarAlertasConsola = function (callback) {
    if(iniciadoConsola){
        return false;
    }
    iniciadoConsola = true;
    var alertasMostradas = []
    var preURL = $('#preURL').attr('value');
   
    var mult = 1;

    (function poll() {
        setTimeout(function () {
            //console.log('llamando...')
            mult = 5;
            $.ajax({
                type: "post",
                url: preURL + "app/handler.asmx/CargarEstadoConsola",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    try {
                        var data = result['d']

                        var d = new Date();
                        var n = d.getMinutes();
                        if (n % 10 == 0) {
                             
                        }
                        else
                        {
                            if (data != null) {
                                $.growl({
                                    icon: 'alertaIcono fa  fa-plug fa-2x ',
                                    title: '<span class="alertaTitulo"><strong>' + "Consola Detenida" + '</strong><span> ',
                                    message: '<span class="alertaMensaje">' + "Última actualización: " + data + '<span> '
                                }, {
                                    type: 'danger',
                                    delay: 30000,
                                    animate: {
                                        enter: 'animated tada',
                                        exit: 'animated hinge'
                                    },
                                    template: '<div data-growl="container" class="alert" role="alert">\
		                                            <button type="button" class="close" data-growl="dismiss">\
			                                            <span aria-hidden="true">×</span>\
			                                            <span class="sr-only">Close</span>\
		                                            </button>\
		                                            <span data-growl="icon" style=""></span>\
		                                            <span data-growl="title"></span>\
		                                            <span data-growl="message"></span>\
		                                            <a href="#" data-growl="url"></a>\
	                                            </div>'

                                });
                            }
                        }
                      
                    }
                    finally {
                        poll();
                    }
                }, dataType: "json"
            });
        }, 60000 * mult);
    })();
}



