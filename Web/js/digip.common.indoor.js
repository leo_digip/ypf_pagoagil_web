
var common = namespace('digip.common');

common.indoor = function(e, p){

	var _self = this;
	var map = null;
	var markers = [];
	var startedDrag = null;
	var params = p;
	var url = params.url;
	var $e = $(e);
	

	_self.clean = function(){
		if(map != null){
			map.remove();
			$e.find('#map').html('');
		}

		 map = null;
		 markers = [];
		 startedDrag = null;
	};

	_self.init = function(p){
		params = p;
		url = params.url;
		map = L.map($e.find('#map')[0], {
			minZoom: 1,
			maxZoom: 4,
			center: [0, 0],
			zoom: 2,
			crs: L.CRS.Simple,
		});

		// dimensions of the image
		//var url = 'images/apple-campus01.png';

		_self.getImageSize(url).done(function(size){
			// calculate the edges of the image, in coordinate space
			var southWest = map.unproject([0, size.h], map.getMaxZoom()-1);
			var northEast = map.unproject([size.w, 0], map.getMaxZoom()-1);
			var bounds = new L.LatLngBounds(southWest, northEast);

			// add the image overlay, 
			// so that it covers the entire map
			L.imageOverlay(url, bounds).addTo(map);

			// tell leaflet that the map is exactly as big as the image
			map.setMaxBounds(bounds);


			if(params.editSingle){
				var coords;
				if(params.singleCoords && params.singleCoords.lat && params.singleCoords.lng){
					coords = params.singleCoords;

				}else{
				    //coords = map.getCenter();
				    coords = { lat: -50, lng: 100 };
				}
				
				
				var options = {
							//pid: guid(),
							pid: 'ID_ANTENA',
							draggable: !params.viewOnly
						};
				_self.insertPoint(coords, options, false, params.label);
				//map.panTo(new L.LatLng(coords.lat, coords.lat));

			}

			if(params.items){
				
			    params.items.forEach(function (item) {
			        
					var options = {
							//pid: guid(),
							pid: item.id,
							draggable: false
						};
					var marker = _self.insertPoint({lat: item.indoorLat, lng: item.indoorLng}, options, false/*, item.descripcion*/);
					marker.bindPopup('<div ><strong><i class="fa fa-cube" style="color: ' + item.activo_color + ';"></i> ' + item.activo_nombre + '</strong>' + '<div><i class="fa fa-bullseye" style="color: ' + item.activo_color + ';"></i>  ' + item.descripcion + '</div></div>');
					marker.on('click', function(e) {
					    params.onItemClick(item);
					});
				});

				if(params.select){
					_self.selectMarker(params.select);
				}

			}
		});

		if(params.editMultiple){
			// Drag & Drop
			$e.find('.items li .drag').draggable({
				helper: 'clone',
				revert: 'invalid',
				start: function(evt, ui) {
					if($(this).hasClass('disabled')){
						//console.log('ya se agregó!');
						return false;
					}

					//console.log('start: '+$(this).find('.caption').html());
					startedDrag = $(this);
				},
				stop: function(evt, ui) {
				}
			});	


			$e.find('#map').droppable(
			{
				drop: function (event, ui) {
					var options = {
						//pid: guid(),
						pid: startedDrag.attr('id'),
						draggable: true
					};

					startedDrag.addClass('disabled');
					
					var currentPos = ui.helper.position();
					_self.insertPoint(map.containerPointToLatLng([currentPos.left - $e.find('#toolbar').width(), currentPos.top]),options, true, startedDrag.find('.caption').html());
				}
			});
		}

		
	};


	_self.getImageSize = function(url){

		var dfd = $.Deferred();
		if(!url){
			dfd.resolve({w: 100, h: 100});
			return dfd;	
		}
		var img = new Image();
		img.onload = function() {
			dfd.resolve( {w: this.width, h:this.height} );
		}
		img.src = url;
		return dfd;
	};


	// INSERT point
	_self.insertPoint = function(position,options, allowRemove, label) {

		var point = L.marker(position,options);
		if(label){
			point.bindLabel(label, { noHide: true });
		}
		
		point.addTo(map);
		if(allowRemove){
			point.bindPopup(
				'<a onClick="deletePoint(\'' + point.options.pid 
					+ '\');" href="#">remover <i class="fa fa-times"></i></a>',
			{
				closeButton: false
			}
			);
		}
		point.on('dragend', function(evt){
			_self.updatePoint(point);
		});
		markers.push(point);

		return point;
	};


	// UPDATE point
	_self.updatePoint = function(point) {


	};


	// DELETE point
	_self.deletePoint = function(pid) {

		$('#'+pid).removeClass('disabled');
		for(i=0;i<markers.length;i++) {
			if(markers[i].options.pid === pid) {
				map.removeLayer(markers[i]);
				markers.splice(i, 1);
			}
		}
	};

	_self.selectMarker = function(id) {
		for(i=0;i<markers.length;i++) {
			if(markers[i].options.pid === id) {
				markers[i].openPopup();
			}
		}
	};

	_self.getCoords = function(){
		var res = {};
		markers.forEach(function(m){
			res[m.options.pid] = m._latlng;
		});
		return res;
	};


	_self.init(p);
};
