﻿

var datatable = null;


$(document).ready(function () {
    //inicializa el datepicker fecha desde:
    var $desde = $('#dt_desde').datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function( selectedDate ) {
            $("#dt_hasta").datepicker("option", "minDate", selectedDate);
        }
    });

    $desde.datepicker('setDate', moment().subtract(1,'d').toDate());

    //inicializa el datepicker fecha hasta:
    var $hasta = $("#dt_hasta").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function (selectedDate) {
            $("#dt_desde").datepicker("option", "maxDate", selectedDate);
        }
    });

    $hasta.datepicker('setDate',new Date());

    // traer todo sin usar fecha desde y hasta:
    $('#chk_todo').change(function () {
        $("#dt_desde, #dt_hasta").prop('disabled', this.checked);
    });



    // slider para el intervalo de refresco:
    var slider = $("#timerSlider").slider({
        value: 120,
        min: 5,
        max: 600,
        step: 1,
        slide: function (event, ui) {
            $("#segundos").html(ui.value);
            refreshTime = ui.value;
            if (myTimer) {
                clearInterval(myTimer);
            }
            if (ui.value > 0) {
                myTimer = setInterval(function () {
                    cargarTabla();
                }, refreshTime * 1000);
            }
        }
    });


    //cuando termina de cargar los filtros, tira la primer carga de la tabla:
    $.when(digip.tags.cargarAreas(), digip.tags.cargarSensores(), digip.tags.cargarTiposMov()).done(function () {
        cargarTabla();
    });


    $('#bt_buscar').click(function () {
        cargarTabla();
        return false;
    });



    //Inicializa el timer de carga de la tabla:
    var myTimer;
    var refreshTime = slider.slider("option", "value");
    if (refreshTime) {
        myTimer = setInterval(function () {
            cargarTabla();
        }, refreshTime * 1000);
    }

});





function difToString(val) {
    var res = '';
    if (val != null) {
        if (val < 60) {
            res = val.toFixed(2) + ' segundos';
            
        } else {
            res = moment.duration(val, 'seconds').humanize();
        }
    }
    return res;
}

function cargarTabla() {
    $('.loading').show();
    $('#div_spinner').removeClass("hidden");
    var sensorId = $('#dd_sensor').val();
    var vDesde = new Date($.now() - $('#cbo_horas').val() * 60 * 60 * 1000);
    var vHasta = new Date($.now());
    
    


    $.when($('.panel-body').fadeTo(100, 0.9), $.ajax({
            type: "post",
            url: $('#preURL').attr('value') +  "app/handler.asmx/CargarEstados",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                desde: vDesde,
                hasta: vHasta,
                todo: $('#chk_todo').prop('checked'),
                areaId: $('#dd_area').val(),
                ubicacionId: $('#dd_ubicacion').val(),
                //tipoId: $('#dd_tipo').val(),
                sensorId: sensorId,
                soloUltimos: $('#chk_ultimos').prop('checked'),
                tipoMov: $('#dd_tipoMov').val()
            }),
            success: function (result) {
                var res = result['d'];
                //alert(result['d']);
                if (res.showStats && res!=null) {
                    //var avgDif, maxDif, minDif = '';
                    $('#stats').show('slow');
                    $('#avgDif').html(difToString(res.avgDif));
                    $('#maxDif').html(difToString(res.maxDif));
                    $('#minDif').html(difToString(res.minDif));
                } else {
                    $('#stats').hide();
                    $('#avgDif').html('');
                    $('#maxDif').html('');
                    $('#minDif').html('');
                }
                var data = res.data;
                var tbody = '';
                for (var i = 0, l = data.length; i < l; i++) {
                    var m = moment(data[i].fecha);
                    //var dif = moment.duration(data[i].dif, 'seconds');
                    var dif = '';
                    if (data[i].dif != null) {
                        if (data[i].dif < 60) {
                            dif = data[i].dif + ' segundos';
                        } else {
                            dif = moment.duration(data[i].dif, 'seconds').humanize()
                        }
                    }
                    //<td>' + new Date(parseInt(data[i].fecha.substr(6))) + '</td>\
                    var val_temp;
                    var val_volt;

                    if (data[i].voltage == null) { val_volt = '-' }
                    if (data[i].voltage == 99999) { val_volt = '-' }
                    else { val_volt = Math.round(data[i].voltage * 100) / 100 }

                    if (data[i].record_temperature == null) { val_temp = '-' }
                    if (data[i].record_temperature == 99999) { val_temp = '-' }
                    else { val_temp = Math.round(data[i].record_temperature * 100) / 100 }
                    
                    
                    tbody += '<tr>\
                            <td>' + data[i].descripcion + '</td>\
                            <td>' + data[i].descripcionMov + '</td>\
                            <td class="fecha" title="' + m.format('YYYY/MM/DD HH:mm:ss') + '">' + m.calendar() + '</td>\
                            <td>' + data[i].area + '</td>\
                            <td>' + data[i].ubicacion + '</td>\
                            <td>' + data[i].antena + '</td>\
                            <td>' + m.format('YYYY/MM/DD HH:mm:ss') + '</td>\
                            <td title="' + data[i].dif + ' segundos.">' +  dif + '</td>\
                            <td >' + data[i].rssi + '</td>\
                            <td >' + (data[i].receiver_counter ? data[i].receiver_counter : '-') + '</td>\
                            <td >' + (data[i].tag_counter ? data[i].tag_counter : '-') + '</td>\
                            <td >' + val_volt + '</td>\
                            <td >' + val_temp + '</td>\
                            <td > <a href="' + $('#preURL').attr('value') + 'APP/stats/detalle_sensores.aspx?id=' + data[i].id + '"><i title="Ver Historial" class="fa fa-calendar verHistorial" rel="' + data[i].id + '"></i></a></td>\
                          </tr>';
                }
                if (datatable) {
                    datatable.fnDestroy();
                }
                $('#gv_estados').html('<thead>\
                                <tr>\
                                    <th>Sensor</th>\
                                    <th>Tipo de Movimiento</th>\
                                    <th>Fecha</th>\
                                    <th>Área</th>\
                                    <th>Ubicación</th>\
                                    <th>Antena</th>\
                                    <th>Fecha_Completa</th>\
                                    <th><abbr title="Minutos transcurridos desde el último movimiento (cualquier Antena) sin contar las alertas de No Encontrado" >Diferencia</abbr></th>\
                                    <th>RSSI</th>\
                                    <th>Receiver #</th>\
                                    <th>Tag #</th>\
                                    <th>Volt</th>\
                                    <th>Temp</th>\
                                    <th></th>\
                                </tr>\
                            </thead>\
                            <tbody>\
                            </tbody>\
                            <tfoot>\
                            </tfoot>');
                $('#gv_estados tbody').html(tbody);
                inicializarPluginTabla();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        }
        )).done(function () {
            $('.panel-body').fadeTo(100, 1);
            $('.loading').hide();
            $('#div_spinner').addClass("hidden");
        })
}


function inicializarPluginTabla() {
    datatable = $('#gv_estados').dataTable({
        "responsive": true,
        "columnDefs": [
            {
                "targets": [  ],
                "visible": false,
                "searchable": false
            },
            { "iDataSort": 6, "aTargets": [2] }
        ],
        "order": [ 2, 'desc' ],
        //"bProcessing": true,
        //"sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar: ",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Ultimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },

            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }



    });


}
    
