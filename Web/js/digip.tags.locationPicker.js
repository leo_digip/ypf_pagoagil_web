﻿var map;
$(document).ready(function () {
	if($('.location_picker').is(":visible")){
		console.log('visible...');
		init();
	}

	$('body').on('click','[href="#nuevo"]' ,function(){
	    if(!map){
	    	init();
	    }
	});

	$('[data-location="location"]').bind("keypress", function (e) {
	    if (e.keyCode == 13) {
	        //$("#btnSearch").attr('value');
	        //add more buttons here
	        return false;
	    }
	});
	
});

function init(){
	if( ($('[data-location="latitud"]').val() == '') || ($('[data-location="longitud"]').val() == '')){
		GetLocation(function(lat, lon){
			$('[data-location="latitud"]').val(lat);
			$('[data-location="longitud"]').val(lon);
			initPicker();
		});
	}
	else
	{
		initPicker();
	}
}

function initPicker(){
	$('.location_picker').locationpicker(
	{
		location: {
			latitude: $('[data-location="latitud"]').val() != ''? parseFloat($('[data-location="latitud"]').val()) : 0, 
			longitude: $('[data-location="longitud"]').val() != ''? parseFloat($('[data-location="longitud"]').val()) : 0, 
		},	
		radius: 0,
		inputBinding: {
			latitudeInput: $('[data-location="latitud"]'),
			longitudeInput: $('[data-location="longitud"]'),
			locationNameInput: $('[data-location="location"]')        
		},
		enableAutocomplete: true,
		onchanged: function(currentLocation, radius, isMarkerDropped) {
				//alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
				console.log("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
				$('.latitud_value').attr('value',currentLocation.latitude);
				$('.longitud_value').attr('value',currentLocation.longitude);
			}
		}
		);
	map = $('.location_picker').locationpicker('map').map;
}
function GetLocation(ready)
{
	if(navigator.geolocation) {
		var options = {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumAge: 0
		};

		function success(pos) {
			var crd = pos.coords;

			console.log('Your current position is:');
			console.log('Latitude : ' + crd.latitude);
			console.log('Longitude: ' + crd.longitude);
			console.log('More or less ' + crd.accuracy + ' meters.');
			ready(crd.latitude,crd.longitude);
		};

		function error(err) {
			console.warn('ERROR(' + err.code + '): ' + err.message);
			ready(0,0);
		};

		navigator.geolocation.getCurrentPosition(success, error, options)
	}
	else {
		ready(0,0);
	}
}