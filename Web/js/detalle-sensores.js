﻿
var data = null;
var map;
var markers = {};
var dataIndex = {};
var indoor;
var idParam
$(document).ready(function () {

    idParam = common.getURLParameter('id');


    //inicializa el datepicker fecha desde:
    var $desde = $('#dt_desde').datepicker({
        defaultDate: "+1d",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function (selectedDate) {
            $("#dt_hasta").datepicker("option", "minDate", selectedDate);
        }
    });

    $desde.datepicker('setDate', moment().toDate());

    //inicializa el datepicker fecha hasta:
    var $hasta = $("#dt_hasta").datepicker({
        defaultDate: "+1d",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function (selectedDate) {
            $("#dt_desde").datepicker("option", "maxDate", selectedDate);
        }
    });

    $hasta.datepicker('setDate', new Date());

    // traer todo sin usar fecha desde y hasta:
    $('#chk_todo').change(function () {
        $("#dt_desde, #dt_hasta").prop('disabled', this.checked);
    });


    //cuando termina de cargar los filtros, tira la primer carga de la tabla:
    $.when(digip.tags.cargarSensores(true)).done(function () {
        if (eval(idParam)) {
            $('#dd_sensor').val(idParam);
            $('#dd_sensor').trigger("chosen:updated");
        }
        cargarTimeline();
    });


    $('#bt_buscar').click(function () {
        if (indoor) {
            indoor.clean();
        }
        cargarTimeline();
        return false;
    });


   // setInterval(function () {         cargarTimeline();    }, 60 * 1000);

    //function refresh() {
    //    if (new Date().getTime() - time >= 60000) {
    //        if (indoor) {
    //            indoor.clean();
    //        }
    //        cargarTimeline();
    //    }
    //    else
    //        setTimeout(refresh, 60000);
    //}

  //  setTimeout(refresh, 60000);


    $('body').on('click', '.timeline-item:not(.desconocida)', function () {

        $('.timeline-item.selected').removeClass('selected');
        $(this).addClass('selected');
        var marker = markers[$(this).attr("data-marker")];
        if (marker) {
            selectMarker(marker, map);
        }

        showIndoor(dataIndex[$(this).attr("data-marker")]);
    });


});



function showIndoor(d) {
    if (indoor) {
        indoor.clean();
    }
    var img = d.indoor;
    if (img != null) {

        indoor = new digip.common.indoor(
            '#indoor',
            {
                editSingle: true,
                viewOnly: true,
                label: d.antenaAlias,
                singleCoords:
                {
                    lat: d.antenaLat,
                    lng: d.antenaLng
                },
                url: $('#preURL').attr('value') + img
            });
    }
}

function cargarTimeline() {

    //$('.loading').show();
    $('.inicial').hide();
    $('.sinDatos').hide();


    map = null;
    markers = {};
    $('#map').html('');

    $.when(/*$('.panel-body').fadeTo(100, 0.9),*/ $.ajax(
        {
            type: "post",
            url: $('#preURL').attr('value') + "app/handler.asmx/CargarTimeline",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({

                desde: $("#dt_desde").datepicker({ dateFormat: 'MM,dd,yyyy' }).val(),
                hasta: $("#dt_hasta").datepicker({ dateFormat: 'MM,dd,yyyy' }).val(),
                todo: $('#chk_todo').prop('checked'),
                sensorId: $('#dd_sensor').val()
            }),
            success: function (result) {
                var $tl = $('.timeline').html('');
                data = result['d'];
                var count = 0;
                if (data.length > 0) {
                    count++;
                    var activo = formatearItemActivo(data[0], count);
                    var next_fecha_hasta = data[0].desde;

                    $tl.append(activo);
                    if (data.length > 1) {
                        var c = 1;
                        for (var i = 1; i < data.length; i++) {
                            data[c].hasta = next_fecha_hasta;
                            next_fecha_hasta = data[c].desde;
                            count++;
                            if (c % 2 == 0) {
                                $tl.append(formatearItemPar(data[c], count))
                            } else {
                                $tl.append(formatearItemImpar(data[c], count))
                            }
                            c++;
                        }
                    }
                    loadMap(data);
                }
                else {
                    $('.sinDatos').show();
                }
            },
            error: function (xhr, status, error) {
                var $tl = $('.timeline').html('');
                alert(error);
            }
        }
        )).always(function () {
            //$('.panel-body').fadeTo(100, 1);
            //$('.loading').hide();
        })
}

function loadMap(data) {
    map = null;
    markers = {};
    $('#map').html('');
    var myLatlng = new google.maps.LatLng(-38, -54);
    var mapOptions = {
        zoom: 4,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.HYBRID
    }
    map = new google.maps.Map($('#map')[0],
        mapOptions);

    var c = 0;
    markers = {};
    dataIndex = {};

    var points = [];

    data.forEach(function (d) {
        c++;
        if (d.latitud != null && d.longitud != null) {
            var myLatlng = new google.maps.LatLng(d.latitud, d.longitud);
            points.push(myLatlng);
            var marker = new google.maps.Marker({
                map: map,
                draggable: false,
                animation: google.maps.Animation.DROP,
                position: myLatlng
            });
            marker.set('digip-id', c);

            google.maps.event.addListener(marker, 'click', function (m, i) {



            });
            markers[c] = marker;
            dataIndex[c] = d;
        }
    });


    var path = new google.maps.Polyline({
        path: points,
        geodesic: true,
        strokeColor: '#92cf5c',
        strokeOpacity: 0.8,
        strokeWeight: 5
    });

    path.setMap(map);

    google.maps.event.addListenerOnce(map, 'idle', function () {
        if (c > 0) {
            selectMarker(markers[1], map);
            showIndoor(dataIndex[1]);
        }
    });


}

function selectMarker(marker, map) {
    var latLng = marker.getPosition(); // returns LatLng object
    map.panTo(latLng); // setCenter takes a LatLng object
    if (map.getZoom() < 15) map.setZoom(15);
    marker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function () { marker.setAnimation(null); }, 2800);
}


function formatearItemActivo(d, i) {

    var icono = "fa-map-marker";
    var clase = '';
    var bg = 'bg-success'
    if (d.ubicacionDesc == '*DESCONOCIDA*') {
        icono = "fa-warning";
        clase = "desconocida"
        d.ubicacionDesc = "SENSOR PERDIDO";
        bg = 'bg-danger';
    }

    var m = moment(d.desde);

    var mHasta = moment(d.hasta);
    var mDesde = moment(d.desde);


    var c = countdown(mDesde.toDate(), mHasta.toDate()).toString()
         .replace('years', 'años')
         .replace('year', 'año')
         .replace('months', 'meses')
         .replace('month', 'mes')
         .replace('days', 'días')
         .replace('day', 'día')
         .replace('hours', 'horas')
         .replace('hour', 'hora')
         .replace('minutes', 'minutos')
         .replace('minute', 'minuto')
         .replace('seconds', 'segundos')
         .replace('second', 'segundo')
         .replace(', and', ' y');



    var html =
        '<article data-marker="' + i + '" class="selected timeline-item active animated pulse ' + clase + '">\
            <div class="timeline-caption">\
                <div class="panel '+ bg + '  no-borders">\
                    <div class="panel-body">\
                        <div class="icono"><i class="fa '+ icono + ' time-icon ' + bg + '"></i></div>\
                        <div class="ubicacion">' + d.ubicacionDesc + '<span class="antena"> <i class="fa fa-location-arrow"></i> ' + d.antenaAlias + '</span></div>\
                        <div class="desde">desde ' + m.calendar() + '<br/>(' + c + ')  </div>\
                    </div>\
                </div>\
             </div>\
       </article>';

    return html
}

function formatearItemImpar(d, i) {

    //a.diff(b, 'days') // 1
    var mDesde = moment(d.desde);
    var mHasta = moment(d.hasta);

    var dur = mHasta.diff(mDesde, 'hours');
    var c = countdown(mDesde.toDate(), mHasta.toDate()).toString()
        .replace('years', 'años')
        .replace('year', 'año')
        .replace('months', 'meses')
        .replace('month', 'mes')
        .replace('days', 'días')
        .replace('day', 'día')
        .replace('hours', 'horas')
        .replace('hour', 'hora')
        .replace('minutes', 'minutos')
        .replace('minute', 'minuto')
        .replace('seconds', 'segundos')
        .replace('second', 'segundo')
        .replace(', and', ' y');


    var icono = "fa-map-marker";
    var clase = '';
    var bg = 'bg-success'
    if (d.ubicacionDesc == '*DESCONOCIDA*') {
        icono = "fa-warning";
        clase = "desconocida"
        bg = 'bg-danger';
        d.ubicacionDesc = "Estuvo Perdido"
    }



    var html =
        '<article data-marker="' + i + '" class="itemSecundario timeline-item animated pulse ' + clase + '">\
                            <div class="timeline-caption">\
                              <div class="panel">\
                                <div class="panel-body">\
                                  <span class="arrow left"></span>\
                                  <span class="timeline-icon"><i class="fa '+ icono + ' time-icon ' + bg + '"></i></span>\
                                    <div class="ubicacion">'+ d.ubicacionDesc + '<span class="antena"> <i class="fa fa-location-arrow"></i> ' + d.antenaAlias + '</span></div>\
                                   <div class="desde">desde ' + mDesde.calendar() + '</div>\
                                   <div class="hasta">hasta ' + mHasta.calendar() + '</div>\
                                  <span class="timeline-date duracion">' + c + '</span>\
                                </div>\
                                </div>\
                                </div>\
                                </article>';

    return html;
}

function formatearItemPar(d, i) {
    var html = formatearItemImpar(d, i);
    html = html.replace('timeline-item', 'timeline-item alt').replace('arrow left', 'arrow right');
    return html;
}


function crearTabla() {
    var $table = $(
                '<table>\
                  <thead>\
                  <tr>\
                    <th>Desde</th>\
                    <th>Hasta</th>\
                    <th>Ubicación</th>\
                  </tr>\
                  </thead>\
                  <tbody>\
                  </tbody>\
                </table>');
    var $tbody = $table.find('tbody');
    data.forEach(function (d) {
        var mDesde = moment(d.desde).format('DD/MM/YYYY HH:mm:ss');
        var mHasta = d.hasta ? moment(d.hasta).format('DD/MM/YYYY HH:mm:ss') : '';
        $tbody.append('<tr><td>' + mDesde + '</td><td>' + mHasta + '</td><td>' + d.ubicacionDesc + '</td></tr>');
    });


    $('#aExportar').attr('value', '<table>' + $table.html() + '</table>');

    return true;
}