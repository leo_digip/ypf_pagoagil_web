﻿var data = null;
var sensores = {};
var sensoresPorUbicacion = {};
var map;
var marcado=null;
var markers = {};
var dataIndex = {};
var indoor;
var currentIndoorId;
var infoWindow;
var selectedTags = [];

function refrescar() {

    var prevData = JSON.stringify(data);
    var selected = $('.selected').attr('id');
    $.when(cargarUbicaciones()).done(function () {
        var currData = JSON.stringify(data);
        if (currData != prevData) {
            render(selected);
        }
    });
}

$(document).ready(function () {
    //window.setInterval(refrescar(), 60000);

    $.when(digip.tags.cargarTags()).done(function (allTags) {
        allTags.forEach(function (t) {
            $('#tags').append('<option value="' + t + '">' + t + '</option>');
        });
        $("#tags").select2({
            placeholder: "filtrar por etiquetas"
        })
        .on("change", function(e) {
            var theSelection = e.val;
            console.log(theSelection);
            selectedTags = theSelection;
            $.when(cargarUbicaciones()).done(function () {
                render();
            });
        });
    });

    $.when(cargarUbicaciones()).done(function() {
        render();
    });

    $('body').on('click', '.sensor', function() {
        selectSensor(this);
    });

    $('.refresh i').click(function(){
        var prevData = JSON.stringify(data);
        var selected = $('.selected').attr('id');
        $.when(cargarUbicaciones()).done(function() {
            var currData = JSON.stringify(data);
            if(currData != prevData){
                    render(selected);
            }
        }); 
    });

    (function poll() {
        setTimeout(function () {
            var prevData = JSON.stringify(data);
            var selected = $('.selected').attr('id');
            $.when(cargarUbicaciones()).done(function() {
                //si cambio algo:
                var currData = JSON.stringify(data);
                if(currData != prevData){
                    render(selected);
                }
                poll();
            });

            }, 1000 * 6000);
    })();



    $('body').on('click', '.activo .top', function () {
        selectTop(this);
    });

    $('body').on('click', '.activo .top .flecha', function () {
        $(this).parents('.activo').find('.sensor').slideToggle();
        $(this).parents('.activo').find('.desplegar').toggle();
        $(this).parents('.activo').find('.colapsar').toggle();
        return false;
        

    });



});


function selectTop(e, fromChild) {
    $('.topSelected').removeClass('.topSelected').css({ 'background-color': 'inherit', 'color': '#2F323D' });


    var bkgColor = $(e).parents('.activo').attr('data-color');
    var candidates = $.xcolor.monochromatic("#FAFAFA", 128);
    var color = '#FAFAFA';
    for (var i = 0; i < candidates.length; i++) {
        c = candidates[i];
        if ($.xcolor.readable(bkgColor, c.getHex())) {
            color = c.getHex();
            //console.log(color);
            break;
        }
    }

    $(e).addClass('topSelected').css({
        'background-color': $(e).parents('.activo').attr('data-color'), 'color': color
    });

    if (!fromChild) {
        if ($(e).parents('.activo').find($('.sensor.selected').attr('id')).length == 0) {
            selectSensor($(e).parents('.activo').find($('.sensor'))[0], true);
        }
    }
}


function selectSensor(e, fromTop) {
    
    $('.sensor.selected').removeClass('selected').css({
        'background-color': 'inherit',
        'color': 'inherit'
    });


    var bkgColor = $(e).attr('data-color');
    var candidates = $.xcolor.monochromatic("#FAFAFA", 128);
    var color = '#FAFAFA';
    for (var i = 0; i < candidates.length; i++) {
        c = candidates[i];
        if ($.xcolor.readable(bkgColor, c.getHex())) {
            color = c.getHex();
            console.log(color);
            break;
        }
    }


    $(e).addClass('selected').css({
        'background-color': $(e).attr('data-color'), 'color': color
    });



    
    var marker = markers[$(e).attr("id")];
    if (marker) {
        selectMarker(marker, map, sensores[$(e).attr("id")]);
    }
    showIndoor(sensores[$(e).attr("id")]);

    if (!fromTop) {
        selectTop($(e).parents('.activo').find('.top'), true);
    }
}

function cargarUbicaciones() {
    return $.ajax({
        type: "post",
        url: $('#preURL').attr('value') + "app/handler.asmx/CargarUltimaUbicacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ tags: selectedTags
        }),
        success: function(result) {
            data = result['d'];
        },
        error: function(xhr, status, error) {
            alert(error);
        }
    });
}

function render(selected) {
    var $lista = $('#sensores');
    $lista.html('');
    sensores = {};
    map = null;
    markers = {};
    sensoresPorUbicacion = {};
    $('#map').html('');
    var myLatlng = new google.maps.LatLng(-38, -54);
    var mapOptions = {
        zoom: 19,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.HYBRID
    }
    map = new google.maps.Map($('#map')[0], mapOptions);
    markers = {};
    var points = [];

    infoWindow = new google.maps.InfoWindow({
      content: 'empty'
    });

    var activos = {};
    data.forEach(function(sensor) {
        var $activo = activos[sensor.activo_id]
        if(!$activo){
            $activo = $('<li class="activo"   data-color="' + sensor.activo_color + '" data-activo="' + sensor.activo_id + '"><div class="top"><i class="fa fa-cube"></i><h1>' + sensor.activo_nombre + '</h1><i style="display: none;" class="flecha desplegar fa fa-chevron-down"></i><i  class="flecha colapsar fa fa-chevron-up"></i></div></li>');
            activos[sensor.activo_id] = $activo;
            

            $activo.css({
                'border-color': sensor.activo_color
            });

            $lista.append($activo);
        }



        sensores[sensor.id] = sensor;
        var m = moment(sensor.fecha);
        var $sensor = 
        $('<div class="sensor" id="' +
         sensor.id +
          '" data-color="' + sensor.activo_color + '"><span class="nombre"><i class="fa fa-bullseye"></i>' +
           sensor.descripcion + 
            '</span><span class="fecha">' + /*m.calendar()*/'' +'</span></div>');
        //$lista.append($sensor);

        /*$sensor.css({
            'background-color': sensor.activo_color
        });*/

        $activo.append($sensor);
        var myLatlng = new google.maps.LatLng(sensor.lat, sensor.lng);
        points.push(myLatlng);

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            icon: ('http://maps.google.com/mapfiles/ms/icons/red-dot.png'),
            animation: google.maps.Animation.DROP,
            position: myLatlng,
            title: sensor.descripcion
        });
        marker.set('digip-id', sensor.id);
        markers[sensor.id] = marker;
        google.maps.event.addListener(marker, 'click', function(m, i) {
            //selectMarker(marker, map, sensores[marker.get('digip-id')]);
            selectSensor($('#' + marker.get('digip-id'))[0]);

        });
        if (!sensoresPorUbicacion[sensor.ubicacionId]) {
            sensoresPorUbicacion[sensor.ubicacionId] = [];
        }
        sensoresPorUbicacion[sensor.ubicacionId].push(sensor);

      
    });

    if(data.length > 0){
        if(selected && $lista.find('#' +  selected).length > 0){
            selectSensor($lista.find('#' +  selected)[0])
        }else{
            selectSensor($lista.find('.sensor').first()[0])
        }
    }
    
}

function showIndoor(sensor) {
    if(currentIndoorId != sensor.ubicacionId){
        currentIndoorId = sensor.ubicacionId; 
        if (indoor) {
            indoor.clean();
        }
        var img = sensor.indoorImg;
        if (img != null) {
            indoor = new digip.common.indoor('#indoor', {
                editSingle: false,
                viewOnly: true,
                /*singleCoords: 
                {
                    lat: d.indoorLat, 
                    lng: d.indoorLng
                }, */
                items : sensoresPorUbicacion[sensor.ubicacionId],
                onItemClick: function(item){
                    selectSensor($('#' + item.id)[0]);
                },
                select: sensor.id,
                url: $('#preURL').attr('value') + img
            });
        }
    }else{
        indoor.selectMarker(sensor.id);
        //indoor.autoPan(true);
    }
}



function getInfo(sensor){

    var m = moment(sensor.fecha);
    /*var c = countdown(m.toDate()).toString()
        .replace('years', 'años')
        .replace('year', 'año')
        .replace('months', 'meses')
        .replace('month', 'mes')
        .replace('days', 'días')
        .replace('day', 'día')
        .replace('hours', 'horas')
        .replace('hour', 'hora')
        .replace('minutes', 'minutos')
        .replace('minute', 'minuto')
        .replace('seconds', 'segundos')
        .replace('second', 'segundo')
        .replace(', and', ' y');*/
    /*var info = $(
        '<div class ="sensorInfo" >\
        <div class="title">' +sensor.activo_nombre + '</div>\
        <span class="sensorInfo" title="sensor"><i class="fa fa-bullseye" ></i>'+sensor.descripcion+'</span>\
        <span class="periodo" title="'+m.calendar()+'"><i class="fa fa-clock-o"></i> '+m.fromNow()+'</span>\
        <span class="area" title="área"><i class="fa fa-location-arrow" ></i>'+sensor.area+'</span>\
        <span class="ubicacion" title="ubicación"><i class="fa fa-building-o" ></i>'+sensor.ubicacion+'</span>\
        <span class="antena" title="antena"><i class="fa fa-wifi" ></i>'+sensor.antena+'</span>\
        </div>')[0];*/

var info = $(
        '<div class ="sensorInfo" >\
        <div class="title">' +sensor.activo_nombre + '</div>\
        <p>El sensor <kbd>'+sensor.descripcion+
        '</kbd> </br>fue detectado en el área <kbd>' + sensor.area +'</kbd>,</br>ubicación: <kbd>'
        + sensor.ubicacion + '</kbd>,</br>por la antena <kbd>'+sensor.antena+ '</kbd></br><kbd title="'+m.calendar()+'">' +m.fromNow()+'</kbd></p></div>')[0];

    return info;
}

function selectMarker(marker, map, sensor) {

    if (marcado==null) {

    }
    else {
       marcado.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
        marcado = marker;
    }


    var latLng = marker.getPosition(); // returns LatLng object
    map.panTo(latLng); // setCenter takes a LatLng object
    if (map.getZoom() < 15) map.setZoom(15);
    marker.setAnimation(google.maps.Animation.BOUNCE);
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
    setTimeout(function() {
        marker.setAnimation(null);
    }, 2800);
    marcado = marker;

    
    var content = getInfo(sensor);
    infoWindow.setContent(content);
    infoWindow.open(map,marker);


}