﻿

var datatable = null;
var tipo = null;

$(document).ready(function () {

    //pasaron filtro por tipo de alerta?
    tipo = common.getURLParameter('tipo')
    noleidas = common.getURLParameter('noleidas')


    //inicializa el datepicker fecha desde:
    var $desde = $('#dt_desde').datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function( selectedDate ) {
            $("#dt_hasta").datepicker("option", "minDate", selectedDate);
        }
    });

    $desde.datepicker('setDate', moment().subtract(1,'w').toDate());

    //inicializa el datepicker fecha hasta:
    var $hasta = $("#dt_hasta").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 2,
        dateFormat: "yy/mm/dd",
        onClose: function (selectedDate) {
            $("#dt_desde").datepicker("option", "maxDate", selectedDate);
        }
    });

    $hasta.datepicker('setDate',new Date());

    // traer todo sin usar fecha desde y hasta:
    $('#chk_todo').change(function () {
        $("#dt_desde, #dt_hasta").prop('disabled', this.checked);
    });



    // slider para el intervalo de refresco:
    var slider = $("#timerSlider").slider({
        value: 0,
        min: 0,
        max: 120,
        step: 1,
        slide: function (event, ui) {
            $("#segundos").html(ui.value);
            refreshTime = ui.value;
            if (myTimer) {
                clearInterval(myTimer);
            }
            if (ui.value > 0) {
                myTimer = setInterval(function () {
                    cargarTabla();
                }, refreshTime * 1000);
            }
        }
    });


    $('#chk_noLeidas').prop('checked', noleidas == 1)

    //cuando termina de cargar los filtros, tira la primer carga de la tabla:
    $.when(digip.tags.cargarAreas(), digip.tags.cargarTiposDeAlerta(tipo),
        digip.tags.cargarSensores(), digip.tags.cargarTiposMov()).done(function () {
        cargarTabla();
    });


    $('#bt_buscar').click(function () {
        cargarTabla();
        return false;
    });


   


    $("#bt_marcarComoLeido").click( function () {
        var dataList = $("#gv_alertas .marcarComoLeido:checked").map(function () {
            return $(this).attr("rel");
        }).get();

        //console.log(dataList.join(","));

        if (dataList.length == 0) {
            bootbox.dialog({
                message: "Debe seleccionar las alertas que desea marcar como leídas.",
                title: "Lectura de Alertas",
                buttons: {
                    success: {
                        label: "Aceptar",
                        className: "btn-primary"
                    }
                }
            });
        } else {
            bootbox.dialog({
                message: "Marcará como leídas las alertas seleccionadas. Esta acción no podrá deshacerse. <br> ¿Desea continuar?",
                title: "Lectura de Alertas",
                buttons: {

                    cancel: {
                        label: "Cancelar",
                        className: "btn-primary"

                    },
                    success: {
                        label: "Si, marcar como leídas",
                        className: "btn-success",
                        callback: function () {
                            $.when($.ajax({
                                type: "post",
                                url: $('#preURL').attr('value') +  "app/handler.asmx/MarcarComoLeidas",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify(
                                    {
                                        ids: dataList,
                                        usuarioId: $('#usuarioId').attr('value')
                                    }),
                                success: function (result) {

                                },
                                error: function (xhr, status, error) {
                                    alert(error);
                                }

                            })).done(function () {
                                cargarTabla();
                            });
                        }
                    }
                }
            });
        }
    });


    //Inicializa el timer de carga de la tabla:
    var myTimer;
    var refreshTime = slider.slider("option", "value");
    if (refreshTime) {
        myTimer = setInterval(function () {
            cargarTabla();
        }, refreshTime * 1000);
    }

});





function cargarTabla() {
        
    $('.loading').show();
    
    
        
    $.when($('.panel-body').fadeTo(100, 0.9), $.ajax({
            type: "post",
            url: $('#preURL').attr('value') +  "app/handler.asmx/CargarAlertas",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                desde: $('#dt_desde').datepicker('getDate'),
                hasta: $('#dt_hasta').datepicker('getDate'),
                todo: $('#chk_todo').prop('checked'),
                areaId: $('#dd_ubicacion').val(),
                ubicacionId: $('#dd_area').val(),
                tipoId: $('#dd_tipoAlerta').val(),
                sensorId: $('#dd_sensor').val(),
                noLeidas: $('#chk_noLeidas').prop('checked'),
                tipoMov: $('#dd_tipoMov').val()
            }),
            success: function (result) {

                var data = result['d'];
                var tbody = '';

                for (var i = 0, l = data.length; i < l; i++) {
                    var m = moment(data[i].fecha);
                   
                    
                    tbody += '<tr>\
                            <td>' + data[i].sensor_descripcion + '</td>\
                            <td>' + data[i].descripcionMov + '</td>\
                            <td>' + data[i].alerta_tipo + '</td>\
                            <td class="fecha" title="' + m.format('YYYY/MM/DD HH:mm:ss') + '">' + m.calendar() + '</td>\
                            <td>' + data[i].area + '</td>\
                            <td>' + data[i].ubicacion + '</td>\
                            <td>' + data[i].antena + '</td>\
                            <td>' + m.format('YYYY/MM/DD HH:mm:ss') + '</td>' + 
                            columnasLectura(data[i]) +
                          '</tr>';
                }

                if (datatable) {
                    datatable.fnDestroy();

                }

                $('#gv_alertas').html('<thead>'+
                              '  <tr>'+
                              '     <th>Sensor</th>'+          //0
                              '     <th>Tipo de Movimiento</th>' +       //1
                              '     <th>Tipo de Alerta</th>' +       //1
                              '     <th>Fecha</th>' +                //2
                              '     <th>Área</th>' +                 //3
                              '     <th>Ubicación</th>' +              //4
                              '     <th>Antena</th>' +               //5
                              '     <th>Fecha (completa)</th>' +     //6
                              '     <th>Leído Por</th>' +            //7
                              '     <th>Fecha Lectura</th>' +        //8
                              '     <th>Fecha Lectura (completa)</th>' + //9
                              '     <th><input type="checkbox" class="seleccionarTodas"/></th>' + //10
                              '  </tr>'+
                            '</thead>'+
                            '<tbody>'+
                            '</tbody>'+
                            '<tfoot>'+
                            '</tfoot>');
                $('#gv_alertas tbody').html(tbody);




                inicializarPluginTabla();
                

            },
            error: function (xhr, status, error) {
                alert(error);
            }

        }
        )).done(function () {
            $('.panel-body').fadeTo(100, 1);
            $('.loading').hide();

        })
    


}

function columnasLectura(data) {
    var leido = data.fechaLectura != null;
    var html = '';
    if (leido) {
        var mLectura = moment(data.fechaLectura);
        html +=
            '<td>' + data.lector_nombre + ' ' + data.lector_nombre + '</td>' +
            '<td class="fecha" title="' + mLectura.format('YYYY/MM/DD HH:mm:ss') + '">' + mLectura.calendar() + '</td>' +
            '<td>' + mLectura.format('YYYY/MM/DD HH:mm:ss') + '</td>' +
            '<td><i class="fa fa-check "></i></td>';
    } else
    {
        html +=
            '<td></td>' +
            '<td class="fecha" title=""></td>' +
            '<td></td>' +
            '<td><input type="checkbox" class="marcarComoLeido" rel="'+data.alerta_id+'"/></td>';
            

    }
    return html;
    
    
}


function inicializarPluginTabla() {
    datatable = $('#gv_alertas').dataTable({
        "responsive": true,
        "columnDefs": [
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [10],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [11],
                "searchable": false,
                "sortable": false
            },
            { "iDataSort": 7, "aTargets": [3] },
            { "iDataSort": 9, "aTargets": [10] }
        ],
        "order": [ 3, 'desc' ],
        //"bProcessing": true,
        //"sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar: ",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Ultimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },

            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }



    });
}
    
