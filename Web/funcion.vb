﻿Imports System.Reflection

Module funcion

    Public Function limpiar(ByVal panel As Panel) As Boolean
        'For Each ctl As Control In CType(CType(page.Master.FindControl("ContentPlaceHolder1"), ContentPlaceHolder).FindControl(panel), Panel).Controls '// Loop thru all Controls on Form.     
        For Each Ctl As Control In panel.Controls
            If TypeOf (Ctl) Is TextBox Then '// Locate Panels.           
                Dim textbox As TextBox = CType(Ctl, TextBox)
                textbox.Text = ""
            End If
        Next
        'Next
        Return True
    End Function

    Public Sub Alert_Modal(ByVal page As Page, body As String, Optional title As String = "Atencion!")
        Dim alertMessage As String = "<script type='text/javascript'> $(function(){	$('#myModal').modal('show'); });</script>"

        Dim html As String
        html = " <div  id='myModal' class='modal fade'>" & vbLf
        html = html & "  <div class='modal-dialog'>" & vbLf
        html = html & "    <div class='modal-content'>" & vbLf
        html = html & "      <div class='modal-header'>" & vbLf
        html = html & "        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>" & vbLf
        html = html & "        <h4 class='modal-title'>" & title & "</h4>" & vbLf
        html = html & "      </div> " & vbLf
        html = html & "      <div class='modal-body'>" & vbLf
        html = html & "        <p>" & body & "</p>" & vbLf
        html = html & "      </div>" & vbLf
        html = html & "      <div class='modal-footer'>" & vbLf
        html = html & "        <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>" & vbLf
        html = html & "      </div>" & vbLf
        html = html & "    </div><!-- /.modal-content -->" & vbLf
        html = html & "  </div><!-- /.modal-dialog -->" & vbLf
        html = html & "</div><!-- /.modal -->" & vbLf


        page.ClientScript.RegisterStartupScript(page.[GetType](), "showAlert", alertMessage & vbLf & html, False)
    End Sub
    Public Function MsgBox2(ByVal pagina As Page, ByVal mensaje As String)
        Dim jv As String = "<script>alert('" & mensaje.Trim.Replace("'", "") & "');</script>"
        ScriptManager.RegisterClientScriptBlock(pagina, GetType(Page), "Mensaje", jv, False)
        Return 0
    End Function


    Public Function GetRandomPasswordUsingGUID(ByVal length As Integer) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function




    Function MensajeHTML(ByVal page As Page, ByVal body As String, Optional ByVal title As String = "Atencion!") As String
        Dim alertMessage As String = "<script type='text/javascript'> $(function(){	$('#myModal').modal('show'); });</script>"
        Dim html As String

        html = "<html lang='en'><head><meta charset='utf-8' />"
        html = html & "  <title>DIGIP TAGS</title>"
        html = html & "  <meta name='description' content='app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav' />"
        html = html & "  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />"
        html = html & " <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/bootstrap.css' " & " type='text/css'  cache='true'  />"
        html = html & "  <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/animate.css' type='text/css'  cache='true'   />"
        html = html & "  <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/font-awesome.min.css' type='text/css'  cache='true'   />"
        html = html & "  <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/font.css' type='text/css'  cache='true'   />"
        html = html & "  <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/plugin.css' type='text/css'  cache='true'   />"
        html = html & "  <link rel='stylesheet' href='" & page.Request.Url.GetLeftPart(UriPartial.Authority) & "/App_Theme_Responsive/css/app.css' type='text/css'  cache='true'   />"
        html = html & "</head><body>" & vbLf

        html = html & " <section class='scrollable wrapper animated bounceInDown'> "
        html = html & " <div class='post-item'>" & vbLf
        html = html & "     <div class='caption wrapper-lg'>" & vbLf
        html = html & "       <h1 class='post-title'>" & title & "</h1>" & vbLf
        html = html & "      <div class='post-sum'>" & vbLf
        html = html & "     <p>" & body & "</p>" & vbLf
        html = html & " </div>" & vbLf
        html = html & "     <div class='line line-lg'></div>"
        html = html & " <div class='text-muted'>"
        html = html & " <i class='fa fa-external-link icon-muted'></i>" & page.Request.Url.ToString
        html = html & " <i class='fa fa-clock-o icon-muted'></i>" & Now
        html = html & " </div>"

        html = html & " </div>"

        html = html & " </body></html>"

        Return html

    End Function




    Function GetFechaConvertString(fecha As DateTime) As String

        If DateDiff(DateInterval.Minute, Now, fecha) < 20 Then
            Return fecha.Minute.ToString & " Minuto"
        ElseIf DateDiff(DateInterval.Hour, Now, fecha) < 2 Then
            Return fecha.Hour.ToString & " hora"
        ElseIf DateDiff(DateInterval.Hour, Now, fecha) < 5 Then
            Return fecha.ToShortTimeString
        ElseIf DateDiff(DateInterval.Day, Now, fecha) < 2 Then
            Return DateDiff(DateInterval.Day, Now, fecha) & "Dia, " & fecha.ToShortTimeString
        Else
            Return fecha.ToShortDateString & " " & fecha.ToShortTimeString
        End If


    End Function

    Sub Getexportar_excel_xml(ByVal dataset As DataSet, ByVal nombre As String)

        Dim context As HttpContext
        context = HttpContext.Current
        context.Response.Clear()
        context.Response.Buffer = True

        Dim xml As String
        Dim sb As New StringBuilder()

        xml = "<?xml version='1.0'?>"
        xml = xml & "<?mso-application progid='Excel.Sheet'?>"
        xml = xml & "<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet'"
        xml = xml & " xmlns:o='urn:schemas-microsoft-com:office:office'"
        xml = xml & " xmlns:x='urn:schemas-microsoft-com:office:excel'"
        xml = xml & " xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet'"
        xml = xml & " xmlns:html='http://www.w3.org/TR/REC-html40'>"
        xml = xml & " <DocumentProperties xmlns='urn:schemas-microsoft-com:office:office' >"
        xml = xml & "  <Author>DIGIP</Author>"
        xml = xml & "  <LastAuthor>DIGIP</LastAuthor>"
        xml = xml & "  <Created>" & Now.Date.ToLocalTime() & "</Created>"
        xml = xml & "  <Version>11.9999</Version>"
        xml = xml & " </DocumentProperties>"
        xml = xml & " <ExcelWorkbook xmlns='urn:schemas-microsoft-com:office:excel'>"
        xml = xml & "  <WindowHeight>13605</WindowHeight>"
        xml = xml & "  <WindowWidth>24915</WindowWidth>"
        xml = xml & "  <WindowTopX>360</WindowTopX>"
        xml = xml & "  <WindowTopY>135</WindowTopY>"
        xml = xml & "  <ActiveSheet>1</ActiveSheet>"
        xml = xml & "  <ProtectStructure>False</ProtectStructure>"
        xml = xml & "  <ProtectWindows>False</ProtectWindows>"
        xml = xml & " </ExcelWorkbook>"
        xml = xml & " <Styles>"
        xml = xml & "  <Style ss:ID='Default' ss:Name='Normal'>"
        xml = xml & "   <Alignment ss:Vertical='Bottom'/>"
        xml = xml & "   <Borders/>"
        xml = xml & "   <Font/>"
        xml = xml & "   <Interior/>"
        xml = xml & "   <NumberFormat/>"
        xml = xml & "   <Protection/>"
        xml = xml & "  </Style>"
        xml = xml & " </Styles>"
        For i As Integer = 0 To dataset.Tables.Count - 1
            xml = xml & "<Worksheet ss:Name='" & dataset.Tables(i).TableName.Trim.Replace(" ", "").Replace("/", "") & "'>"
            xml = xml & " <Table ss:ExpandedColumnCount='" & dataset.Tables(i).Columns.Count & "' ss:ExpandedRowCount='" & dataset.Tables(i).Rows.Count + 1 & "' x:FullColumns='1'"
            xml = xml & "  x:FullRows='1' ss:DefaultColumnWidth='60'>"
            'Columnas
            xml = xml & "  <Row>"
            For k As Integer = 0 To dataset.Tables(i).Columns.Count - 1
                xml = xml & "   <Cell><Data ss:Type='String'>" & dataset.Tables(i).Columns(k).ColumnName.Trim.ToString.Normalize & "</Data></Cell>"
            Next
            xml = xml & "  </Row>"
            'Filas
            For j As Integer = 0 To dataset.Tables(i).Rows.Count - 1
                xml = xml & "  <Row>"
                For k As Integer = 0 To dataset.Tables(i).Columns.Count - 1
                    If IsNumeric(dataset.Tables(i).Rows(j)(k).ToString.Trim) Then
                        xml = xml & "   <Cell><Data ss:Type='Number'>" & dataset.Tables(i).Rows(j)(k).ToString.Trim.Replace("&", "").Replace(">", "").Replace("<", "").Replace("´", "").ToString.Normalize & "</Data></Cell>"
                    Else
                        xml = xml & "   <Cell><Data ss:Type='String'>" & dataset.Tables(i).Rows(j)(k).ToString.Trim.Replace("&", "").Replace(">", "").Replace("<", "").Replace("´", "").ToString.Normalize & "</Data></Cell>"
                    End If
                Next
                xml = xml & "  </Row>"
            Next
            xml = xml & " </Table>"
            xml = xml & " <WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'>"
            xml = xml & "  <PageSetup>"
            xml = xml & "   <Header x:Margin='0'/>"
            xml = xml & "   <Footer x:Margin='0'/>"
            xml = xml & "   <PageMargins x:Bottom='0.984251969' x:Left='0.78740157499999996'"
            xml = xml & "    x:Right='0.78740157499999996' x:Top='0.984251969'/>"
            xml = xml & "  </PageSetup>"
            xml = xml & "  <Selected/>"
            xml = xml & "  <ProtectObjects>False</ProtectObjects>"
            xml = xml & "  <ProtectScenarios>False</ProtectScenarios>"
            xml = xml & " </WorksheetOptions>"
            xml = xml & "</Worksheet>"
        Next
        xml = xml & "</Workbook>"
        context.Response.ContentType = "application/vnd.ms-excel"
        'context.Response.ContentEncoding = System.Text.Encoding.UTF7
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" & nombre & " " & Now.Date.ToShortDateString.Replace("/", "-") & ".xls")
        ' context.Response.End()
        context.Response.Output.Write(xml)
        context.Response.Flush()
        context.Response.End()

    End Sub

    Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
        Dim dt As New DataTable()
        Dim propiedades As PropertyInfo() = GetType(T).GetProperties
        For Each p As PropertyInfo In propiedades
            dt.Columns.Add(p.Name)
        Next
        For Each item As T In list
            Dim row As DataRow = dt.NewRow
            For Each p As PropertyInfo In propiedades
                row(p.Name) = p.GetValue(item, Nothing)
            Next
            dt.Rows.Add(row)
        Next
        Return dt
    End Function

    Sub GetExportar_csv(ByVal table As DataTable, ByVal nombre As String)

        Dim context As HttpContext

        context = HttpContext.Current
        context.Response.Clear()
        context.Response.Buffer = True
        Dim sb As New StringBuilder()

        For Each col As DataColumn In table.Columns

            'context.Response.Write(col.ColumnName.Normalize.ToLower & ";")

            sb.Append(col.ColumnName & ";")
        Next

        sb.Append(Environment.NewLine)

        For Each row As DataRow In table.Rows

            For i As Integer = 0 To table.Columns.Count - 1
                sb.Append(row(i).ToString.Replace(vbCrLf, " ").Replace(";", String.Empty) + ";")
            Next
            sb.Append(Environment.NewLine)
        Next

        context.Response.ContentType = "application/CSV"
        context.Response.ContentEncoding = System.Text.Encoding.GetEncoding(1252)
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" & nombre & ".csv")
        ' context.Response.End()


        context.Response.Output.Write(sb.ToString())
        context.Response.Flush()
        context.Response.End()

    End Sub

    Sub ReloadParentWindow(ByVal page As Page)
        Dim scr As String = "<script type='text/javascript'> $(function(){	window.opener.location = window.opener.location });</script>"
        page.ClientScript.RegisterStartupScript(page.[GetType](), "ReloadOpenerWindow", scr & vbLf, False)
    End Sub

End Module
