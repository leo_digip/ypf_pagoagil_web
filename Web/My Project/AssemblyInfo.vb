Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado
<Assembly: AssemblyTitle("YPF_Mobile")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Digip")> 
<Assembly: AssemblyProduct("Digip YPF_Mobile")> 
<Assembly: AssemblyCopyright("Copyright © Digip 2016")> 
<Assembly: AssemblyTrademark("Digip")> 

<Assembly: ComVisible(False)> 

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("71cb2958-9770-4672-a8a2-11961380655e")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el carácter '*', como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.9.0.*")> 

<Assembly: NeutralResourcesLanguageAttribute("es-AR")> 