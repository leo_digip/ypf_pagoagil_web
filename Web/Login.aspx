﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  CodeBehind="Login.aspx.vb" Inherits="YPF_Mobile.Login" %>
    <!DOCTYPE html>
    <html lang="es">

    <head id="Head1">
        <meta charset="utf-8" />
        <title>YPF Mobile</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <%--<link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/bootstrap.css" ) %>" type="text/css" />--%>
            <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/animate.css ")%>" type="text/css" />
            <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font-awesome.min.css ")%>" type="text/css" />
            <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font.css ")%>" type="text/css" />
            <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/plugin.css ")%>" type="text/css" />
            <%--<link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/app.css" ) %>" type="text/css" />--%>
                <link rel="stylesheet" href="/App_Theme_Responsive/css/bootstrap.css" type="text/css" />
                <link rel="stylesheet" href="App_Theme_Responsive/css/app.css" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datatables/datatables.css ")%>" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css ")%>" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/select2/select2.css ")%>" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css ")%>" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datepicker/datepicker.css ")%>" type="text/css" />
                <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/slider/slider.css ")%>" type="text/css" />
                <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/jquery.min.js ")%>"></script>
                <!-- Bootstrap -->
                <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/bootstrap.js ")%>"></script>
                <!--[if lt IE 9]>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/respond.min.js" )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/html5.js"  )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/fix.js"  )%>"></script>
  <![endif]-->
                <SCRIPT lang="JavaScript">
                    function popup(URL, ancho, alto, id) {
                        day = new Date();
                        eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=" + ancho + ", height =" + alto + ",left = 625,top = 210');");
                    }
                </script>
                <style type="text/css">
                    .jqstooltip {
                        position: absolute;
                        left: 0px;
                        top: 0px;
                        visibility: hidden;
                        background: rgb(0, 0, 0) transparent;
                        background-color: rgba(0, 0, 0, 0.6);
                        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
                        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
                        color: white;
                        font: 10px arial, san serif;
                        text-align: left;
                        white-space: nowrap;
                        padding: 5px;
                        border: 1px solid white;
                        z-index: 10000;
                    }
                    
                    .jqsfield {
                        color: white;
                        font: 10px arial, san serif;
                        text-align: left;
                    }
                </style>
                <style>
                    html,
                    body {
                        height: 100%;
                        margin: 0;
                        padding: 0
                    }
                    
                    .container-fluid {
                        height: 100%;
                        display: table;
                        width: 100%;
                        padding: 0;
                    }
                    
                    .row-fluid {
                        height: 100%;
                        display: table-cell;
                        vertical-align: middle;
                    }
                    
                    .centering {
                        float: none;
                        margin: 0 auto;
                    }
                </style>
    </head>

    <body style="background-image: url('/ypf_2.jpg');background-size:cover; background-position:center">
        <div class="container-fluid  ">
            <div class="row-fluid">
                <div class="centering">
                    <form id="padre" role="form" runat="server">
                        <div class="animated fadeInDownBig " id="pnl_login" runat="server">
                            <div class="col-md-4 col-md-offset-4">
                                <div id="div-validate1"> <img src="ypf_logo.png" class="img-responsive center-block m-b" style="height: 33%; width: 33%;" />
                                    <section class="pane l  box-shadow no-borders">
                                        <header class="panel-heading text-center   ">
                                            <h2>Bienvenido</h2> </header>
                                        <div class="panel-body bg-primary dker">
                                            <p class="text-muted text-center">Ingrese su DNI y contraseña para continuar</p>
                                            <div class="form-group">
                                                    <asp:TextBox ID="tb_usuario" required="true" placeholder="DNI..." runat="server" CssClass="form-control h3" TabIndex="1"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                    <asp:TextBox ID="tb_pass" data-required="true" placeholder="Contraseña..." runat="server" CssClass="form-control" TextMode="Password" TabIndex="2"></asp:TextBox>
                                            </div>
                                            <div class="form-group"><br />
                                                <asp:Button ID="btn_ingreso" CssClass="btn btn-primary btn-lg btn-block" EnableTheming="false" TabIndex="3" runat="server" Text="Ingresar"></asp:Button>
                                            </div>
                                            <div class="form-group  text-center hidden">
                                                <a href="fRegistracion.aspx" class="btn btn-sm btn-info dk  "> <i class="fa fa-pencil"></i> Olvidé mi contraseña </a>
                                            </div> <small>Version del Software: <asp:Literal ID="lt_version" runat="server"></asp:Literal></small> </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class=" animated fadeInUpBig hidden" id="pnl_pass" runat="server">
                            <div class="col-md-4 col-md-offset-4">
                                <div id="div1">
                                    <section class="panel  box-shadow">
                                        <header class="panel-heading text-center bg-light lter">
                                            <h3>Blanqueo de Contraseña</h3> </header>
                                        <div class="panel-body">
                                            <p class="text-muted">Bienvenido
                                                <asp:Literal ID="lt_nyacompleto" runat="server"></asp:Literal> tu contraseña fue blanqueada debes cambiarla para poder continuar.</p>
                                            <div class="line line-dashed"></div>
                                            <p class="text-muted"> Recuerda que en DIGIP nadie tiene acceso a las contraseñas, ya que se guardar automaticamente encriptadas.</p>
                                            <div class="line line-dashed"></div>
                                            <div class="form-group"> <label>Nueva Contraseña</label>
                                                <asp:TextBox ID="tb_pasnueva" name="tb_pasnueva" placeholder="Nueva contraseña..." runat="server" TextMode="Password" data-required="true" ClientIDMode="Static" data-minlength="8" ControlToValidate="tb_pasnueva" CausesValidation="True"></asp:TextBox>
                                            </div>
                                            <div class="form-group"> <label>Repetir la nueva contraseña</label>
                                                <asp:TextBox ID="tb_pasnueva2" name="tb_pasnueva2" placeholder="Repetir nueva contraseña..." runat="server" data-minlength="8" data-equalto="#tb_pasnueva" data-required="true" TextMode="Password" ClientIDMode="Static"></asp:TextBox>
                                            </div>
                                            <div class="form-group"><br />
                                                <asp:Button ID="bt_aplicar" runat="server" Text="Aplicar cambios" CausesValidation="false" EnableTheming="false" CssClass="btn btn-success btn-lg btn-block"></asp:Button>
                                            </div>
                                            <div class="form-group  text-center"><br />
                                                <h6>¿ Problema para el ingreso ?<br /> </h6><small>Envie un email a Mesadeayuda@digip.com.ar</small> <br /> </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- app -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.js ")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.plugin.js ")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.data.js ")%>"></script>
        <!-- fuelux -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.js ")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/underscore-min.js ")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/jquery.pjax.js ")%>"></script>
        <!-- datatables -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datatables/jquery.dataTables.min.js ")%>"></script>
        <!-- Easy Pie Chart -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/charts/easypiechart/jquery.easy-pie-chart.js ")%>"></script>
        <!-- datepicker -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datepicker/bootstrap-datepicker.js ")%>"></script>
        <!-- slider -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/slider/bootstrap-slider.js ")%>"></script>
        <!-- file input -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/file-input/bootstrap.file-input.js ")%>"></script>
        <!-- combodate -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/moment.min.js ")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/combodate/combodate.js ")%>"></script>
        <!-- select2 -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/select2/select2.min.js ")%>"></script>
        <!-- wysiwyg -->
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/jquery.hotkeys.js")%>"></script>
        <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/bootstrap-wysiwyg.js")%>"></script>
        <%--<script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/demo.js"  )%>"></script>--%>
            <!-- markdown -->
            <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/markdown/epiceditor.min.js")%>"></script>
            <%--<script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/markdown/demo.js"  )%>"></script>--%> </body>

    </html>