﻿Imports YPF_Mobile.ServiceReference1

Public Class fComprobante
    Inherits System.Web.UI.Page
    'Public comp As Byte()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim w As New ServiceReference1.PagoAgilClient
            Dim seg As IdentificacionUsuarioResult = Session("tUsuario")
            Dim comprobante = w.ObtenerComprobante(seg.seguridad, Long.Parse(Request.QueryString("idComprobante")))
            Dim c As Comprobante
            c = comprobante.comprobante
            Session("comprobante") = c.comprobante

            ltl_idcomprobante.Text = "ID Comprobante: " & c.idComprobante
            ltl_nombre.Text = "Nombre archivo: " & c.nombre
            ltl_transaccion.Text = "ID Transaccion: " & c.idTransaccion
        End If

    End Sub

    Sub ss() Handles btn_comp.ServerClick

        Dim comp As Byte()
        comp = CType(Session("comprobante"), Byte())

        Response.ClearHeaders()
        Response.Clear()
        Response.AddHeader("Content-Type", "application/pdf")
        Response.AddHeader("Content-Length", comp.Length.ToString())
        Response.AddHeader("Content-Disposition", "inline; filename=sample.pdf")
        Response.BinaryWrite(comp)
        Response.Flush()
        Response.End()


    End Sub

End Class