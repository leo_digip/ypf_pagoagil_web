﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fMapa.aspx.vb" Inherits="YPF_Mobile.fMapa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contenido" runat="server">

    <script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    var im = 'http://www.robotwoods.com/dev/misc/bluecircle.png';
    function locate() {
        navigator.geolocation.getCurrentPosition(initialize, fail);
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocalizacion no soportada en este navegador.");
        }
    }
    function showPosition(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        map.setCenter(new google.maps.LatLng(lat, lng));
    }

    function initialize(position) {
        var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var mapProp = {
            center: myLatLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var userMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: im,
            title: 'Mi ubicación actual'
        });



        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-34.584885, -58.444528),
            map: map,
            title: 'YPF Opessa Dorrego', icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'

        });



        var marker1 = new google.maps.Marker({ position: new google.maps.LatLng(-34.616153, -58.441900), map: map, title: 'YPF'});
        var marker2 = new google.maps.Marker({ position: new google.maps.LatLng(-34.609937, -58.430060), map: map, title: 'YPF' });
        var marker3 = new google.maps.Marker({ position: new google.maps.LatLng(-34.622621, -58.390355), map: map, title: 'YPF' });
        var marker4 = new google.maps.Marker({ position: new google.maps.LatLng(-34.596243, -58.426079), map: map, title: 'YPF' });
        var marker5 = new google.maps.Marker({ position: new google.maps.LatLng(-34.622453, -58.400562), map: map, title: 'YPF' });
         
        var contentString = '<section  style="color:black"><h5 class="text-info"><i class="fa fa-gear"></i>&nbsp;YPF Opessa Dorrego</h5>' +
            '<p><i class="fa fa-credit-card"></i> &nbsp; Cuenta con Pago Ágil</p>' +
            '<p><i class="fa fa-coffee"></i> &nbsp; Cuenta con YPF Full</p>' +
            '<p><i class="fa fa-rss"></i> &nbsp; Cuenta con WIFI</p>' +
            '<p><i class="fa fa-cogs"></i> &nbsp; Cuenta con YPF Boxes</p>' +
            '</section>'


        //'<p><label class="badge text-xs bg-success">promo</label><span>Hoy 15% OFF Con todas las tarjetas de crédito</span></p>'+

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

         
    }
    google.maps.event.addDomListener(window, 'load', locate);

    function fail() {
        alert('Geolocalizacion no detectada, verifique si tiene habilitado el GPS');
    }



    

</script>


    <section id="content">
    <section class="vbox">
            <section class="scrollable wrapper">
                <div class="col-xs-12 no-padder">

                <div class=" animated fadeInUpBig ">
                    <div class="col-md -7 no-padder no-borders">
                        <section class="pan el bg-primary dker no-borders">
                            <div class="panel-heading dker h4 no-borders">Estaciones mas cercanas</div>
                  
                                    <div id="googleMap" style="width:100%;height:480px;"></div>

                        </section>
                    </div>
                         
                        
                        </div>
                        </div>
            </section>

        </section>
        </section>



</asp:Content>
