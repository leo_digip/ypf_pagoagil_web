﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fTarjetas.aspx.vb" Inherits="YPF_Mobile.fTarjetas" %>

    <asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">

        <section id="content">

            <section class="vbox">
                <section class="scrollable wrapper">
                    <div class="col-xs-12 no-padder">


                        <div class=" animated fadeInUpBig ">
                            <div class="col-md -7 no-padder no-borders">
                                <section class="pan el bg-primary dker no-borders">
                                    <div class="panel-heading dker h4 no-borders">Seleccionar medio de pago</div>
                                    <div class="panel-body no-borders">

                                        <asp:Literal ID="ltl_tarjetas" runat="server"></asp:Literal>

<%--                                        <a href="/app/fProcesandoPago.aspx" class="font-semibold">
                                            <article class="media padder-v">
                                                <div class="pull-left  ">
                                                    <span class="h1">
                                                    <i class="fa fa-credit-card text-warning"></i>
                                                </span>  
                                                </div>
                                                <div class="media-body m-l">
                                                    <p class="h2 m-b-none m-l">VISA</p>
                                                    <p class="m-b-none text-muted text-xs pull- right m-l"> XXXX-XXXX-XXXX-2157</p>
                                                </div>
                                            </article>
                                        </a>
                                        <div class="line line-dashed"></div>
                                        <a href="/app/fProcesandoPago.aspx" class="font-semibold">
                                            <article class="media padder-v">
                                                <div class="pull-left  ">
                                                    <span class="h1">
                                                    <i class="fa fa-credit-card text-info"></i>
                                                </span>
                                                </div>
                                                <div class="media-body m-l">
                                                    <p class="h2 m-b-none m-l">AMEX</p>
                                                    <p class="m-b-none text-muted text-xs pull- right m-l"> XXXX-XXXX-XXXX-3325</p>
                                                </div>
                                            </article>
                                        </a>
                                        <div class="line line-dashed"></div>
                                        <a href="/app/fProcesandoPago.aspx" class="font-semibold">
                                            <article class="media padder-v">
                                                <div class="pull-left  ">
                                                    <span class="h1">
                                                    <i class="fa fa-credit-card text-danger"></i>
                                                </span>
                                                </div>
                                                <div class="media-body m-l">
                                                    <p class="h2 m-b-none m-l">DINERS</p>
                                                    <p class="m-b-none text-muted text-xs pull- right m-l"> XXXX-XXXX-XXXX-1254</p>
                                                </div>
                                            </article>
                                        </a>
                                        <div class="line line-dashed"></div>
                                        <a href="/app/fProcesandoPago.aspx" class="font-semibold">
                                            <article class="media padder-v">
                                                <div class="pull-left">
                                                    <span class="h1">
                                                    <i class="fa fa-credit-card text-warning"></i>
                                                </span>
                                                </div>
                                                <div class="media-body m-l">
                                                    <p class="h2 m-b-none m-l">NARANJA</p>
                                                    <p class="m-b-none text-muted text-xs pull- right m-l"> XXXX-XXXX-XXXX-7854</p>
                                                </div>
                                            </article>
                                        </a>--%>




                                    </div>
                                </section>
                            </div>


                        </div>
                    </div>
                </section>

            </section>
        </section>


    </asp:Content>