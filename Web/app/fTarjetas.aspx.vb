﻿Imports YPF_Mobile.ServiceReference1

Public Class fTarjetas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then
            cargarTarjetas()
        End If
    End Sub

    Sub cargarTarjetas()
        Dim htm As String = ""
        Dim sTarjeta As String
        Dim sNumero As String
        Dim idTarjeta As String
        Dim lTarjetas As List(Of TarjetasDefault)
        lTarjetas = CType(Session("tUsuario"), IdentificacionUsuarioResult).Tarjetas.ToList





        For a = 0 To lTarjetas.Count - 1
            sTarjeta = lTarjetas(a).tipoTarjeta
            sNumero = lTarjetas(a).NroTarjetaOF
            idTarjeta = lTarjetas(a).IdTarjeta
            htm = htm & "          <a href='/app/fProcesandoPago.aspx?idt=" & idTarjeta & "' class='font-semibold'>"
            htm = htm & "              <article class='media padder-v'>"
            htm = htm & "                  <div class='pull-left  '>"
            htm = htm & "                      <span class='h1'>"
            htm = htm & "                      <i class='fa fa-credit-card text-warning'></i>"
            htm = htm & "                  </span>"
            htm = htm & "                  </div>"
            htm = htm & "                  <div class='media-body m-l'>"
            htm = htm & "                      <p class='h2 m-b-none m-l'>" & sTarjeta & "</p>"
            htm = htm & "                      <p class='m-b-none text-muted text-xs pull- right m-l'>" & sNumero & "</p>"
            htm = htm & "                  </div>"
            htm = htm & "              </article>"
            htm = htm & "          </a>"
            If a < lTarjetas.Count - 1 Then
                htm = htm & "      <div class='line line-dashed'></div>"
            End If
        Next
        ltl_tarjetas.Text = htm


    End Sub

End Class