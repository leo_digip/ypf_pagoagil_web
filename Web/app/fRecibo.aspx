﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fRecibo.aspx.vb" Inherits="YPF_Mobile.fRecibo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contenido" runat="server">



    <style>
        img-invert {
            -webkit-filter: invert(1);
            filter: invert(1);
            }

    </style>

 <div class="animated fadeInUpBig "  id="pnl_login" runat="server">  
     
        <div class="col-md-4 col-md-offset-4">

    
        <section class="panel box-shadow no-borders bg-dark">
            <header class="panel-header wrapper hidden">
                <div>
                    <img alt="YPF" src="ypf_logo.png" class="img-responsive  " style="height: 25%; width: 25%;" />
                </div>
            </header>
            
            <div class="panel-body  bg-white">
                <p class=" h4 text-center">Recibo 00031-005456442</p>
                
                <div class="text-xs col-xs-12 no-padder">
                    <div class="col-xs-4 no-padder">Fecha: 14/01/2016</div>
                    <div class="col-xs-4 no-padder">Hora: 10:45</div>
                    <div class="col-xs-4 no-padder">Operador: Javier Perez</div>
                </div>
                <br />
                <div class="col-xs-12 h4 text-center m-t no-padder">
                    <p>Venta de combustibles</p>
                </div>
                <div class="line line-dashed"></div>
                <div class="col-xs-12">
                    <p>INFINIA</p>
                </div>
                <div class="col-xs-6">13,68 x 29,23 Lts. </div>
                <div class="col-xs-2">
                    <p>&nbsp;</p>
                </div>
                <div class="col-xs-4 text-right">$ 400,00</div>

                <br />
                <div class="line line-dashed"></div>
                <div class="col-xs-12 text-right m-t">
                    <strong>TOTAL $ 400,00</strong>
                </div>

                <br />
                <div class="col-xs-12 m-t">
                    <p>Medio de pago: VISA</p>
                    <p>Nro.Operación: 231504 </p>
                </div>

                <div class="line line-dashed"></div>
                <div class="col-xs-12 text-right m-t text-center">
                    <strong>Muchas gracias por su compra</strong>
                </div>
                <div class="line line-dashed"></div>

                <a href="fCarga.aspx" class="btn btn-primary btn-lg center-block">Volver</a>
            </div>
        </section>    

    </div>
 
    </div>

</asp:Content>
