﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fComprobantesPendientes.aspx.vb" Inherits="YPF_Mobile.fComprobantesPendientes" %>
    <asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">

        <section id="content">

            <section class="vbox">
                <section class="scrollable wrapper">
                    <div class="col-xs-12 no-padder">


                        <div class=" animated fadeInUpBig ">
                            <div class="col-md -7 no-padder no-borders">
                                <section class="pan el bg-primary dker no-borders">
                                    <div class="panel-heading dker h4 no-borders">Cargas pendientes</div>
                                    <div class="panel-body no-borders">




                                        <section class="panel no-borders">
                                    <section class="hbox">
                                    <section class="scrollable wrapper">
									<div class="table-responsive m-l-sm m-r-sm">

                                          <section class="hbox">
                                    <section class="scrollable wrapper">

										<asp:GridView ID="gv_grilla" runat="server" data-ride="datatables" 
											CssClass="table bg-white text-sm" EnableTheming="False" 
											GridLines="None" AutoGenerateColumns="False">
											<Columns>
                                                <asp:BoundField DataField="fum" HeaderText="Fecha" />
												<asp:BoundField DataField="idTransaccion" HeaderText="Nº" />
                                                <asp:BoundField DataField="monto" HeaderText="Monto" />
                                                <asp:BoundField DataField="estado" HeaderText="Estado" />
                                                
                                                
											</Columns>
										</asp:GridView>

                                        </section>
                                        </section>

									</div>
									<!--/ContentTemplate>
										</UpdatePanel-->
                                    </section>
                                        </section>
								</section>
							 


                                    </div>
                                </section>
                            </div>


                        </div>
                    </div>
                </section>

            </section>
        </section>


    </asp:Content>