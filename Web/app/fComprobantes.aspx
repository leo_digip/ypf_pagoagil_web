﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fComprobantes.aspx.vb" Inherits="YPF_Mobile.fComprobantes" %>

    <asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">

        <section id="content">

            <section class="vbox">
                <section class="scrollable wrapper">
                    <div class="col-xs-12 no-padder">


                        <div class=" animated fadeInUpBig ">
                            <div class="col-md -7 no-padder no-borders">
                                <section class="pan el bg-primary dker no-borders">
                                    <div class="panel-heading dker h4 no-borders">Historial de consumos</div>
                                    <div class="panel-body no-borders">




                                        <section class="panel no-borders">
                                    <section class="hbox">
                                    <section class="scrollable wrapper">
									<div class="table-responsive m-l-sm m-r-sm">

                                          <section class="hbox">
                                    <section class="scrollable wrapper">


										<asp:GridView ID="gv_grilla" runat="server" data-ride="datatables" 
											CssClass="table bg-white text-xs" EnableTheming="False" 
											GridLines="None" AutoGenerateColumns="False">
											<Columns>
                                                <asp:TemplateField>
													<ItemTemplate>
                                                        <asp:LinkButton ID="lnk_ver" CommandName="ver" CommandArgument='<%# Eval("idComprobante")%>' runat="server">
                                                            <i class="fa fa-search-plus"></i>&nbsp;Ver...
                                                        </asp:LinkButton>
														<div class="hidden ">
															<a href="#" ><i class="fa fa-search-plus"></i> Ver...</a>
															<ul class="dropdown-menu pull-right">
																<li>
																	
																</li>
																<li class="divider"></li>
																<li>
																</li>
															</ul>
														</div>
													</ItemTemplate>
												</asp:TemplateField>
                                                
                                                <asp:BoundField DataField="idComprobante" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" HeaderText="IdComprobante" />
												<asp:BoundField DataField="idTransaccion" HeaderText="Nº" />
                                                <asp:BoundField DataField="fechaTransaccion" HeaderText="Fecha" />
                                                <asp:BoundField DataField="monto" HeaderText="$" />
                                                
											</Columns>
										</asp:GridView>

                                        </section>
                                        </section>

									</div>
									<!--/ContentTemplate>
										</UpdatePanel-->
                                    </section>
                                        </section>
								</section>
							 


                                    </div>
                                </section>
                            </div>


                        </div>
                    </div>
                </section>

            </section>
        </section>


    </asp:Content>