﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Default.Master" CodeBehind="home.aspx.vb" Inherits="YPF_Mobile.home" %>

<asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">

    <section id="content">

        <section class="vbox">
            <section class="scrollable wrapper"  >
                <div class="col-sm-7 no-padder">

                        <section class="pan el  animated fadeInUpBig">
	                        <div class="panel-body  bg-primary dker">
		                        <div class="clearfix m-b col-sm-12 no-padder">
                                    <div class="col-sm-1 no-padder">
			                            <span class="pull-left">
                                            <i class="fa fa-clock-o text-warning fa-3x m-t-xs"></i> 
			                            </span>
                                    </div>
			                        <div class="clear col-sm-10  no-padder">
                                        <h1 class="m-b-none m-t-sm"><strong>YPF Mobile</strong></h1>
                                        <small class="block h3 text-muted">Bienvenido</small> 
			                        </div>
		                        </div>
		                        <h4 class="pull-right"> 
                                    <i class="fa fa-desktop"></i>
                                    Consola de Administración </h4>
	                        </div>
	                        <footer class="panel-footer pos-rlt"> <span class="arrow top"></span>
		                        <div class="pull-out">
			                        <p class="h6 m-l m-t">
                                        Si necesitás ayuda podés contactarnos por estos medios<br />
                                        Email: <a href="mailto:mesadeayuda@digip.com.ar">Mesadeayuda@digip.com.ar</a><br />
                                        Telefono: 011.5199.5725
                                    </p>
                                    <h4>
                                        <asp:Literal ID="lt_usuario_agente" runat="server"></asp:Literal>
                                    </h4>
		                        </div>
	                        </footer>
                        </section>
                            
                            
                             
                             

                <div class=" ">
                    <div class="col-md -7 no-padder no-borders">
                        <section class="pan el bg-primary dker m-t no-borders">
                            <div class="panel-heading dker h4 no-borders">Actividad</div>
                            <div class="panel-body no-borders">
                                <a href="/app/fMovimientos.aspx" target="_blank" class="font-semibold">
                                <article class="media">
                                    <div class="pull-left wrapper">
                                        <span class="fa  fa-bullseye fa-2x"></span>
                                    </div>
                                    <div class="media-body">
                                        Movimientos
                                        <div class="text-xs block m-t-xs">Información sobre las actividades de entrada/salida de cada puesto.</div>
                                    </div>
                                </article>
                                </a>
                                <div class="line line-dashed"></div>
                                <a href="/app/fResumenUsuarios.aspx" target="_blank" class="font-semibold">
                                <article class="media">
                                    <div class="pull-left wrapper">
                                        <span class="fa fa-map-marker fa-2x"></span>
                                    </div>
                                    <div class="media-body">
                                        Resumen por usuario
                                        <div class="text-xs block m-t-xs">Informe consolidado de actividad.</div>
                                    </div>
                                </article>
                                </a>
                            </div>
                        </section>
                    </div>
                         
                        
                        </div>
                        </div>
            </section>

        </section>
    </section>
    

</asp:Content>
