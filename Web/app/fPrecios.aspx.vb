﻿Public Class fPrecios1
    Inherits System.Web.UI.Page
     
    Sub click_100() Handles btn_100.ServerClick
        Session("Monto") = "100"
        Server.Transfer("~/app/fTarjetas.aspx", False)
    End Sub

    Sub click_200() Handles btn_200.ServerClick
        Session("Monto") = "200"
        Server.Transfer("~/app/fTarjetas.aspx", False)
    End Sub

    Sub click_300() Handles btn_300.ServerClick
        Session("Monto") = "300"
        Server.Transfer("~/app/fTarjetas.aspx", False)
    End Sub

    Sub click_400() Handles btn_400.ServerClick
        Session("Monto") = "400"
        Server.Transfer("~/app/fTarjetas.aspx", False)
    End Sub

    Sub click_lleno() Handles btn_lleno.ServerClick
        Session("Monto") = "TL"
        Server.Transfer("~/app/fTarjetas.aspx", False)
    End Sub

End Class