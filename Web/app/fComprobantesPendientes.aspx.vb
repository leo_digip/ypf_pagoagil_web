﻿Imports YPF_Mobile.ServiceReference1

Public Class fComprobantesPendientes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim w As New ServiceReference1.PagoAgilClient
            Dim seg As IdentificacionUsuarioResult = Session("tUsuario")
            Dim lc As New List(Of TransaccionModel)

            Dim r As New TransaccionesResult
            r = w.ObtenerTransaccionesPendientes(seg.seguridad)
            If Not IsNothing(r.transacciones) Then
                lc = r.transacciones.ToList()

                For Each ss In lc
                    ss.monto = ss.monto / 100

                Next

                gv_grilla.DataSource = lc
                gv_grilla.DataBind()
                gv_grilla.UseAccessibleHeader = True
                If lc.Count > 0 Then gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
            Else
                gv_grilla.UseAccessibleHeader = True
                If lc.Count > 0 Then gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
            End If



        End If

    End Sub

    Private Sub gv_grilla_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gv_grilla.RowCommand
        If e.CommandName = "ver" Then
            Server.Transfer("~/app/fComprobante.aspx?idComprobante=" & e.CommandArgument, False)

            'ElseIf e.CommandName = "activar" Then
            '    t.Id = e.CommandArgument
            '    'c.EnableEntidad(t)
            '    cargo_grilla()
        End If
    End Sub

End Class