﻿Public Class fPuestos
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            cargo_grilla()
        End If
    End Sub

    Sub cargo_grilla()
        Dim l As New List(Of tPuestos)
        Dim c As New cPuestos
        l = c.getList
        gv_grilla.DataSource = l
        gv_grilla.DataBind()
        gv_grilla.UseAccessibleHeader = True
        If l.Count > 0 Then gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
    End Sub

    Protected Sub btn_agregar_Click(sender As Object, e As EventArgs) Handles btn_agregar.Click
        Dim t As New tPuestos
        Dim c As New cPuestos
        t.ACTIVO = chk_activo.Checked
        t.DESCRIPCION = txt_descripcion.Text
        c.addObjeto(t)

        gv_grilla.UseAccessibleHeader = True
        gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Private Sub gv_grilla_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gv_grilla.RowCommand
        Dim c As New cPuestos
        Dim t As New tPuestos
        If e.CommandName = "editar" Then
            'rellenar datos
            div_main.Visible = False
            t.ID = e.CommandArgument
            t = c.GetObjeto(t.ID)
            chk_ed_activo.Checked = t.ACTIVO
            txt_ed_descripcion.Text = t.DESCRIPCION
            txt_ed_id.Text = t.ID
            div_edic.Visible = True

            'ElseIf e.CommandName = "activar" Then
            '    t.Id = e.CommandArgument
            '    c.EnableEntidad(t)
            '    cargo_grilla()
        End If
    End Sub

    Private Sub btn_ed_guardar_Click(sender As Object, e As EventArgs) Handles btn_ed_guardar.Click
        Dim c As New cPuestos
        Dim t As New tPuestos
        Dim r As New cGlobales.respuesta
        'asignacion de contPuestos a objeto
        t.ACTIVO = chk_ed_activo.Checked
        t.DESCRIPCION = txt_ed_descripcion.Text
        t.ID = txt_ed_id.Text
        r = c.SetObjeto(t)
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Protected Sub btn_ed_cancelar_Click(sender As Object, e As EventArgs) Handles btn_ed_cancelar.Click
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

End Class