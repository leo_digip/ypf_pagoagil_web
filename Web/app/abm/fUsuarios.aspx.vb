﻿Public Class fUsuarios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dt_fechanac.Text = Now.ToString("yyyy-MM-dd")
            cargo_grilla()
            cargo_combos()
            txt_password.Attributes("type") = "password"
            txt_ed_password.Attributes("type") = "password"


        End If
    End Sub



    Sub cargo_combos()
        Dim ce As New NEGOCIO.cRoles

        cbo_rol.DataSource = ce.getList()
        cbo_rol.DataSource = ce.getList()
        cbo_rol.DataValueField = "id"
        cbo_rol.DataTextField = "descripcion"
        cbo_rol.DataBind()
        cbo_rol.Items.Add(" ")
        cbo_rol.SelectedValue = " "

        cbo_ed_rol.DataSource = ce.getList()
        cbo_ed_rol.DataSource = ce.getList()
        cbo_ed_rol.DataValueField = "id"
        cbo_ed_rol.DataTextField = "descripcion"
        cbo_ed_rol.DataBind()
        cbo_ed_rol.Items.Add(" ")
        cbo_ed_rol.SelectedValue = " "


    End Sub


    Sub cargo_grilla()
        Dim l As New List(Of tusuarios)
        Dim c As New cUsuarios
        l = c.getList
        gv_grilla.DataSource = l
        gv_grilla.DataBind()
        gv_grilla.UseAccessibleHeader = True
        If l.Count > 0 Then gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
    End Sub

    Protected Sub btn_agregar_Click(sender As Object, e As EventArgs) Handles btn_agregar.Click
        Dim t As New tusuarios
        Dim c As New cUsuarios
        t.activo = chk_activo.Checked
        t.fechaalta = Date.Now
        t.fechaultmodif = Date.Now
        t.apellido = txt_apellido.Text
        t.nombre = txt_nombre.Text
        t.DOC = txt_doc.Text()
        t.CALLE = txt_calle.Text
        t.FECHA_NACIMIENTO = dt_fechanac.Text
        t.ALTURA = txt_altura.Text
        t.PASSWORD = txt_password.Text
        t.LOCALIDAD = txt_localidad.Text
        t.TELEFONO = txt_cel.Text
        t.EMAIL = txt_email.Text
        c.addObjeto(t)
        gv_grilla.UseAccessibleHeader = True
        gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Private Sub gv_grilla_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gv_grilla.RowCommand
        Dim c As New cUsuarios
        Dim t As New tusuarios
        If e.CommandName = "editar" Then
            'rellenar datos
            div_main.Visible = False
            t.Id = e.CommandArgument
            t = c.GetObjeto(t.Id)
            chk_ed_activo.Checked = t.activo
            txt_ed_apellido.Text = t.apellido
            txt_ed_nombre.Text = t.NOMBRE
            cbo_ed_rol.SelectedValue = t.ID_ROL
            txt_ed_password.Text = t.PASSWORD
            txt_ed_doc.Text = t.DOC
            txt_ed_calle.Text = t.CALLE
            txt_ed_altura.Text = t.ALTURA
            dt_ed_fechanac.Text = t.FECHA_NACIMIENTO.Value.ToString("yyyy-MM-dd")
            txt_ed_telefono.Text = t.TELEFONO
            txt_ed_localidad.Text = t.LOCALIDAD
            txt_ed_id.Text = t.id
            div_edic.Visible = True

        ElseIf e.CommandName = "activar" Then
            t.Id = e.CommandArgument
            'c.EnableEntidad(t)
            cargo_grilla()
        End If
    End Sub

    Private Sub btn_ed_guardar_Click(sender As Object, e As EventArgs) Handles btn_ed_guardar.Click
        Dim c As New cUsuarios
        Dim t As New tusuarios
        Dim r As New cGlobales.respuesta
        'asignacion de controles a objeto
        t.Activo = chk_ed_activo.Checked
        t.apellido = txt_ed_apellido.Text
        t.nombre = txt_ed_nombre.Text
        t.DOC = txt_ed_doc.Text
        t.CALLE = txt_ed_calle.Text
        t.ALTURA = txt_ed_altura.Text
        t.LOCALIDAD = txt_ed_localidad.Text
        t.ID_ROL = cbo_ed_rol.SelectedValue
        t.FECHAULTMODIF = Date.Now
        t.TELEFONO = txt_ed_telefono.Text
        t.FECHA_NACIMIENTO = dt_ed_fechanac.Text
        t.PASSWORD = txt_ed_password.Text
        t.EMAIL = txt_ed_email.Text
        t.id = txt_ed_id.Text
        r = c.SetObjeto(t)
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Protected Sub btn_ed_cancelar_Click(sender As Object, e As EventArgs) Handles btn_ed_cancelar.Click
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub



End Class