﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fPerfil.aspx.vb" Inherits="YPF_Mobile.fPerfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contenido" runat="server">
    <section id="content">
            <header class="header bg-primary padder-v padder animated bounceInDown">
            <div class="col-xs-12 h4 no-padder">
                <i class="fa fa-user"></i>&nbsp;Mi perfil
            </div>
        </header>


<section class="scrollable wrapper">
<div class="row">
	
		<section class="animated fadeInRight" id="vbox">
            <!-- edicion -->
			<div class="wrapper col-sm-12" id="div_edic"  visible="true"  runat="server">
				<header class="header h5">
					<ul class="nav nav-tabs nav-white">
						<li class="active" id="tab_editar">
							<a href="#div_editar" data-toggle="tab"><i class="fa fa-pencil"></i> Editar mis datos personales</a>
						</li>
					</ul>
				</header>
				<div class="tab-content">
					<div class="tab-pane panel no-borders active" id="div_editar">
						<section class="panel-body">
							<div class ="bs-example form-horizontal" >
								<div class ="form-group hidden">
									<label class ="col-sm-1">Nro</label>   
									<div class ="col-sm-2">
										<asp:TextBox ID="txt_ed_id" CssClass="form-control" runat="server" Enabled="False"></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Apellido</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_apellido" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Nombre</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_nombre" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
								</div>
                                 
                                <div class ="form-group">
									<label class ="col-sm-1">DNI</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_doc" CssClass="form-control" Enabled="false"  runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Fecha nacimiento</label>   
									<div class ="col-sm-5">
                                        <asp:TextBox ID="dt_ed_fechanac" runat="server"  type="date"></asp:TextBox>
									</div>
								</div>
                                <div class ="form-group">
									<label class ="col-sm-1">Contraseña web</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_password" CssClass="form-control" runat="server" ></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Calle</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_calle" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Altura</label>   
									<div class ="col-sm-1">
										<asp:TextBox ID="txt_ed_altura" CssClass="form-control" runat="server"></asp:TextBox>
									</div>

                                    <label class ="col-sm-1">Localidad</label>   
									<div class ="col-sm-3">
										<asp:TextBox ID="txt_ed_localidad" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Telefono</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_telefono" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Email</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_email" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
								</div>
                                
								<asp:Button ID="btn_ed_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" UseSubmitBehavior="true"></asp:Button>
                                <asp:Button ID="btn_ed_cancelar" CssClass="btn btn-danger" runat="server" Text="Cancelar" UseSubmitBehavior="true"></asp:Button>
							</div>
						</section>
					</div>
				</div>
			</div><!-- edicion -->

		</section>

</div><!-- row -->
</section>

    </section>
</asp:Content>


