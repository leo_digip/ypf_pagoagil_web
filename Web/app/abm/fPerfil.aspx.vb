﻿Public Class fPerfil
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'dt_fechanac.Text = Now.ToString("yyyy-MM-dd")
            'cargo_grilla()
            '  cargo_combos()
            'txt_password.Attributes("type") = "password"
            txt_ed_password.Attributes("type") = "password"

            Dim c As New cUsuarios
            Dim t As tUsuarios = CType(Session("tUsuarios"), tUsuarios)
            'rellenar datos


            '            t = c.GetObjeto(t.ID)

            'chk_ed_activo.Checked = t.ACTIVO
            txt_ed_apellido.Text = t.APELLIDO
            txt_ed_nombre.Text = t.NOMBRE
            txt_ed_password.Text = t.PASSWORD
            txt_ed_doc.Text = t.DOC
            txt_ed_calle.Text = t.CALLE
            txt_ed_altura.Text = t.ALTURA
            dt_ed_fechanac.Text = t.FECHA_NACIMIENTO.Value.ToString("yyyy-MM-dd")
            txt_ed_telefono.Text = t.TELEFONO
            txt_ed_localidad.Text = t.LOCALIDAD
            txt_ed_id.Text = t.ID
            txt_ed_email.Text = t.EMAIL


        End If
    End Sub

    Private Sub btn_ed_guardar_Click(sender As Object, e As EventArgs) Handles btn_ed_guardar.Click
        Dim c As New cUsuarios
        Dim t As New tUsuarios
        Dim r As New cGlobales.respuesta
        'asignacion de controles a objeto
        t.APELLIDO = txt_ed_apellido.Text
        t.NOMBRE = txt_ed_nombre.Text
        t.DOC = txt_ed_doc.Text
        t.CALLE = txt_ed_calle.Text
        t.ALTURA = txt_ed_altura.Text
        t.LOCALIDAD = txt_ed_localidad.Text
        t.ID_ROL = 2 'usuario final
        t.FECHAULTMODIF = Date.Now
        t.TELEFONO = txt_ed_telefono.Text
        t.FECHA_NACIMIENTO = dt_ed_fechanac.Text
        t.PASSWORD = txt_ed_password.Text
        t.EMAIL = txt_ed_email.Text
        t.NYACOMPLETO = txt_ed_nombre.Text & " " & txt_ed_apellido.Text
        t.ID = CType(Session("tUsuarios"), tUsuarios).ID   ' txt_ed_id.Text
        r = c.SetObjeto(t)
        Session("tUsuarios") = t
        Server.TransferRequest("/app/homeusuario.aspx", False)
    End Sub

    Protected Sub btn_ed_cancelar_Click(sender As Object, e As EventArgs) Handles btn_ed_cancelar.Click
        Server.TransferRequest("/app/homeusuario.aspx", False)
    End Sub



End Class