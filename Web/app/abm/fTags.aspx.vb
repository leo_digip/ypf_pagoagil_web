﻿Public Class fTags
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            cargo_grilla()
            cargo_combos()
        End If
    End Sub

    Sub cargo_combos()
        Dim ce As New NEGOCIO.cUsuarios
        cbo_doc.DataSource = ce.getList()
        cbo_doc.DataValueField = "DOC"
        cbo_doc.DataTextField = "doc"
        cbo_doc.DataBind()
        cbo_doc.Items.Add(" ")
        cbo_doc.SelectedValue = " "

        cbo_ed_doc.DataSource = ce.getList()
        cbo_ed_doc.DataValueField = "DOC"
        cbo_ed_doc.DataTextField = "doc"
        cbo_ed_doc.DataBind()
        cbo_ed_doc.Items.Add(" ")
        cbo_ed_doc.SelectedValue = " "

    End Sub

    Sub cargo_grilla()
        Dim l As New List(Of tTags)
        Dim c As New cTags
        l = c.getList
        gv_grilla.DataSource = l
        gv_grilla.DataBind()
        gv_grilla.UseAccessibleHeader = True
        If l.Count > 0 Then gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
    End Sub

    Protected Sub btn_agregar_Click(sender As Object, e As EventArgs) Handles btn_agregar.Click
        Dim t As New tTags
        Dim c As New cTags
        t.activo = chk_activo.Checked
        t.fechaalta = Date.Now
        t.fechamodif = Date.Now
        t.tagid = txt_tagid.Text
        t.activo = chk_activo.Checked
        t.pin = txt_pin.Text
        t.verificador = txt_verificador.Text
        t.tarjeta = txt_tarjeta.Text
        t.doc = cbo_doc.SelectedValue
        c.addObjeto(t)
        gv_grilla.UseAccessibleHeader = True
        gv_grilla.HeaderRow.TableSection = TableRowSection.TableHeader
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Private Sub gv_grilla_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gv_grilla.RowCommand
        Dim c As New cTags
        Dim t As New tTags
        If e.CommandName = "editar" Then
            'rellenar datos
            div_main.Visible = False
            t.tagid = e.CommandArgument
            t = c.GetObjeto(t.tagid)
            chk_ed_activo.Checked = t.activo
            txt_ed_tagid.Text = t.tagid
            txt_ed_pin.Text = t.pin
            cbo_ed_doc.SelectedValue = t.doc
            txt_ed_verificador.Text = t.verificador
            txt_ed_tarjeta.Text = t.tarjeta
            div_edic.Visible = True

            'ElseIf e.CommandName = "activar" Then
            ' t.Id = e.CommandArgument
            ' 'c.EnableEntidad(t)
            'cargo_grilla()
        End If
    End Sub

    Private Sub btn_ed_guardar_Click(sender As Object, e As EventArgs) Handles btn_ed_guardar.Click
        Dim c As New cTags
        Dim t As New tTags
        Dim r As New cGlobales.respuesta
        'asignacion de controles a objeto
        t.Activo = chk_ed_activo.Checked
        t.tagid = txt_ed_tagid.Text
        t.pin = txt_ed_pin.Text
        t.verificador = txt_ed_verificador.Text
        t.tarjeta = txt_ed_tarjeta.Text
        t.doc = cbo_ed_doc.SelectedValue
        t.fechamodif = Date.Now
        r = c.SetObjeto(t)
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub

    Protected Sub btn_ed_cancelar_Click(sender As Object, e As EventArgs) Handles btn_ed_cancelar.Click
        Server.TransferRequest(Request.Url.AbsolutePath, False)
    End Sub





End Class