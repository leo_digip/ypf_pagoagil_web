﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Default.Master" CodeBehind="fTags.aspx.vb" Inherits="YPF_Mobile.fTags" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contenido" runat="server">
    <section id="content">
            <header class="header bg-primary padder-v padder animated bounceInDown">
            <div class="col-xs-12 h4 no-padder">
                <i class="fa fa-Tags"></i>&nbsp;Tags
            </div>
        </header>


<section class="scrollable wrapper">
<div class="row">
	
		<section class="animated fadeInRight" id="vbox">
			<div class="col-sm-12" id="div_main" runat="server">
                <header class="header h5">
					<ul class="nav nav-tabs nav-white">
						<li class="active" id="tab_lista">
							<a href="#div_listado" data-toggle="tab"><i class="fa fa-th"></i> Lista</a>
						</li>
						<li class="" id="tab_nuevo">
							<a href="#div_nuevo" data-toggle="tab" ><i class="fa fa-file"></i> Agregar</a>
						</li>
					</ul>
				</header>
				<div class="tab-content">
					<div class="tab-pane active text-sm " id="div_listado">
						
						 
						<section class="panel no-borders">
                            <section class="hbox">
                                <section class="scrollable wrapper">
									<div class="table-responsive m-l-sm m-r-sm">

                                        <section class="hbox">
                                    <section class="scrollable wrapper">


										<asp:GridView ID="gv_grilla" runat="server" data-ride="datatables" 
											CssClass="table text-xs bg-white" EnableTheming="False" 
											GridLines="None" AutoGenerateColumns="False">
											<Columns>
                                                <asp:BoundField DataField="TagId" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" HeaderText="Nro" />
												<asp:BoundField DataField="Tarjeta" HeaderText="Tarjeta" />
                                                <asp:BoundField DataField="doc" HeaderText="DNI" />
                    							<asp:TemplateField HeaderText="Estado">
													<ItemTemplate>
														<asp:Label ID="Label1"  Visible='<%# Eval("activo")%>' CssClass="label bg-success" runat="server" Text="Activo"></asp:Label>
														<asp:Label ID="Label2"  Visible='<%# Not Eval("activo")%>' CssClass="label bg-danger" runat="server" Text="Inactivo"></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField>
													<ItemTemplate>
														<div class="btn-group">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil"></i> Editar</a>
															<ul class="dropdown-menu pull-right">
																<li>
																	<asp:LinkButton ID="ltl_editar" CommandName="editar" CommandArgument='<%# eval("tagid") %>' runat="server">Editar...</asp:LinkButton>
																</li>
																<li class="divider hidden"></li>
																<li>
																	<asp:LinkButton ID="ltl_desactivar" CssClass="hidden" CommandName="activar" CommandArgument='<%# eval("tagid") %>'   runat="server">Activar/Desactivar</asp:LinkButton>
																</li>
															</ul>
														</div>
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</asp:GridView>

                                        </section>
                                        </section>

									</div>
									<!--/ContentTemplate>
										</UpdatePanel-->
                                    </section>
                                        </section>
								</section>
							 
					 
					</div>
                    <!-- nuevo -->
					<div class="tab-pane panel no-borders" id="div_nuevo">
						<section class="panel-body">
							<div class ="bs-example form-horizontal" >
                                <div class ="form-group">
									<label class ="col-sm-1">Tag ID</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_tagid" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Nº Tarjeta</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_tarjeta" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
								</div>
                                 
                                <div class ="form-group">
                                    <label class ="col-sm-1">PIN</label>   
									<div class ="col-sm-5">
                                        <asp:TextBox ID="txt_pin" runat="server"  ></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Verificador</label>   
									<div class ="col-sm-5">
                                        <asp:TextBox ID="txt_verificador" runat="server"  ></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">DNI propietario</label>   
									<div class ="col-sm-5">
										<asp:DropDownList ID="cbo_doc" class="form-control input-sm" runat="server"></asp:DropDownList>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Activo</label>   
									<div class ="col-sm-11">
										<asp:CheckBox ID="chk_activo"  runat="server" Checked="True"></asp:CheckBox>
										<i class="fa fa-info-circle"></i>
										<i class="text-muted">Al desmarcar el casillero, este item no podra ser utilizado para nuevos elementos en el sistema</i>
									</div>
								</div>
								<asp:Button ID="btn_agregar" CssClass="btn btn-success" runat="server" Text="Agregar" UseSubmitBehavior="True"></asp:Button>
							</div>
						</section>
					</div>
				</div>
			</div>

            <!-- edicion -->

			<div class="wrapper col-sm-12" id="div_edic"  visible="false"  runat="server">
				<header class="header h5">
					<ul class="nav nav-tabs nav-white">
						<li class="active" id="tab_editar">
							<a href="#div_editar" data-toggle="tab"><i class="fa fa-pencil"></i> Editar</a>
						</li>
					</ul>
				</header>
				<div class="tab-content">
					<div class="tab-pane panel no-borders active" id="div_editar">
						<section class="panel-body">
							<div class ="bs-example form-horizontal" >
                                <div class ="form-group">
									<label class ="col-sm-1">Tag ID</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_tagid" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Nº Tarjeta</label>   
									<div class ="col-sm-5">
										<asp:TextBox ID="txt_ed_tarjeta" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
								</div>
                                 
                                <div class ="form-group">
                                    <label class ="col-sm-1">PIN</label>   
									<div class ="col-sm-5">
                                        <asp:TextBox ID="txt_ed_pin" runat="server"  ></asp:TextBox>
									</div>
                                    <label class ="col-sm-1">Verificador</label>   
									<div class ="col-sm-5">
                                        <asp:TextBox ID="txt_ed_verificador" runat="server"  ></asp:TextBox>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">DNI propietario</label>   
									<div class ="col-sm-5">
										<asp:DropDownList ID="cbo_ed_doc" class="form-control input-sm" runat="server"></asp:DropDownList>
									</div>
								</div>

                                <div class ="form-group">
									<label class ="col-sm-1">Activo</label>   
									<div class ="col-sm-11">
										<asp:CheckBox ID="chk_ed_activo"  runat="server" Checked="True"></asp:CheckBox>
										<i class="fa fa-info-circle"></i>
										<i class="text-muted">Al desmarcar el casillero, este item no podra ser utilizado para nuevos elementos en el sistema</i>
									</div>
								</div>

								<asp:Button ID="btn_ed_guardar" CssClass="btn btn-success" runat="server" Text="Guardar" UseSubmitBehavior="true"></asp:Button>
                                <asp:Button ID="btn_ed_cancelar" CssClass="btn btn-danger" runat="server" Text="Cancelar" UseSubmitBehavior="true"></asp:Button>
							</div>
						</section>
					</div>
				</div>
			</div><!-- edicion -->

		</section>

</div><!-- row -->
</section>

    </section>
</asp:Content>


