﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fComprobante.aspx.vb" Inherits="YPF_Mobile.fComprobante" %>
    <asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">
        <section id="content">
            <section class="vbox">
                <section class="scrollable wrapper">
                    <div class="col-xs-12 no-padder">
                        <div class=" animated fadeInUpBig ">
                            <div class="col-md -7 no-padder no-borders">
                                <section class="pan el bg-primary dker no-borders">
                                    <div class="panel-heading dker h4 no-borders">Comprobantes</div>
                                    <div class="panel-body no-borders">
                                        <section class=" no-borders">
                                    <section class="hbox">
                                    <section class="scrollable  ">
									<div class=" v ">
                                        <section class="hbox">
                                            <section class="scrollable ">

                                                <section class="">
                                                    <div class="panel-body ">
                                                        <img alt="YPF" src="ypf_logo.png" style="width: 150px;" class="img-responsive center-block m-b m-t" />
                                                        <br />
                                                        <asp:Literal ID="ltl_resultado" runat="server"></asp:Literal>
                
                        
                                                        <div class="center-block  text-center  ">
                                                            <div class="form-group">
                                                                <p class="h4">
                                                                    <asp:Literal ID="ltl_idcomprobante" runat="server"></asp:Literal>
                                                                </p>
                                                                <p class="h4">
                                                                    <asp:Literal ID="ltl_transaccion" runat="server"></asp:Literal>
                                                                </p>
                                                                <p class="h4">
                                                                    <asp:Literal ID="ltl_nombre" runat="server"></asp:Literal>
                                                                </p>
                                                                <br />
                                                                <button runat="server" id="btn_comp" class="btn btn-info btn-lg"><i class="fa fa-file"></i>&nbsp;Ver ticket</button>
                                                            </div>

                                                            <br />
                                                            <a class="btn btn-primary btn-lg center-block" href="/fMenu.aspx">Continuar</a>
                                                        </div>
                                                    
                                                    </div>
                                                </section>    


                                                <asp:Literal ID="ltl_comprobante" runat="server"></asp:Literal>
                                         
                                            </section>
                                        </section>
									</div>
									<!--/ContentTemplate>
										</UpdatePanel-->
                                    </section>
                                        </section>
								</section>
							 


                                    </div>
                                </section>
                            </div>


                        </div>
                    </div>
                </section>

            </section>
        </section>


    </asp:Content>