﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="fPagoOK.aspx.vb" Inherits="YPF_Mobile.fPagoOK" %>


<!DOCTYPE html>
<html lang="es">
<head  id="Head1">
    <meta charset="utf-8" />
    <title>YPF Mobile</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <%--<link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/bootstrap.css" ) %>" type="text/css"   />--%>
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/animate.css" ) %>" type="text/css"    />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font-awesome.min.css"  ) %>" type="text/css"   />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/font.css" ) %>" type="text/css" />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/plugin.css" ) %>" type="text/css"  />
    <%--<link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/css/app.css" ) %>" type="text/css"  />--%>

    <link rel="stylesheet" href="/App_Theme_Responsive/css/bootstrap.css" type="text/css"   />
    <link rel="stylesheet" href="/App_Theme_Responsive/css/app.css" type="text/css"  />
  
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datatables/datatables.css" ) %>" type="text/css"  />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css" ) %>" type="text/css"  />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/select2/select2.css" ) %>" type="text/css"  />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.css" ) %>" type="text/css"  />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datepicker/datepicker.css" ) %>" type="text/css"  />
    <link rel="stylesheet" href="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/slider/slider.css" ) %>" type="text/css"  />
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/jquery.min.js" )%>"></script>
    <!-- Bootstrap -->
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/bootstrap.js" )%>"></script>


  <!--[if lt IE 9]>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/respond.min.js" )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/html5.js"  )%>"></script>
    <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/ie/fix.js"  )%>"></script>
  <![endif]-->
  
 <SCRIPT lang="JavaScript">
     function popup(URL, ancho, alto, id) {
         day = new Date();
         eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=" + ancho + ", height =" + alto + ",left = 625,top = 210');");
     }</script>

  
  
  <style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>
<style>
html, body{height:100%; margin:0;padding:0}
 
.container-fluid{
  height:100%;
  display:table;
  width: 100%;
  padding: 0;
}
 
.row-fluid {height: 100%; display:table-cell; vertical-align: middle;}
 
.centering {
  float:none;
  margin:0 auto;
}


</style>

</head>

<body style="background-image: url('/ypf_1.jpg');background-size:cover " >



<div class="container-fluid  ">
    <div class="row-fluid">
        <div class="centering" >
  

   

<form id="padre" role="form"     runat="server">

 <div class="animated fadeInUpBig "  id="pnl_login" runat="server">  
     
        <div class="col-md-4 col-md-offset-4">

    
        <section class="pa nel box-shadow no-borders">
            <div class="panel-body bg-dark dker">
                <img alt="YPF" src="ypf_logo.png" class="img-responsive center-block m-b m-t" style="height: 50%; width: 50%;" />
                <br />
                <asp:Literal ID="ltl_resultado" runat="server"></asp:Literal>
                
                        
                <div class="center-block  text-center  ">
                    <p class="h3">
                        <asp:Literal ID="ltl_operacion" runat="server"></asp:Literal>
                        <asp:Literal ID="ltl_message" runat="server"></asp:Literal>
                    </p>

                    <br />
                    <a class="btn btn-primary btn-lg center-block" href="/fMenu.aspx">Continuar</a>
                </div>

              <%--  <script>
                    setTimeout(myFunction, 2222000);
                    function myFunction() {
                        window.location = "fRecibo.aspx";
                    }
                </script>--%>

                <br />  
                 
            </div>
        </section>    

    </div>
 
    </div>
</form>



    </div>
        </div>
    </div>
        

 

   <!-- app -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.js")%>"></script>
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.plugin.js")%>"></script>
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/app.data.js")%>"></script>
    <!-- fuelux -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/fuelux/fuelux.js")%>"></script>
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/underscore-min.js")%>"></script>
 <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/jquery.pjax.js")%>"></script>
  <!-- datatables -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datatables/jquery.dataTables.min.js")%>"></script>
  <!-- Easy Pie Chart -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/charts/easypiechart/jquery.easy-pie-chart.js")%>"></script>

  <!-- datepicker -->
  
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/datepicker/bootstrap-datepicker.js")%>"></script>
  <!-- slider -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/slider/bootstrap-slider.js")%>"></script>
  <!-- file input -->  
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/file-input/bootstrap.file-input.js")%>"></script>
  <!-- combodate -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/libs/moment.min.js")%>"></script>
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/combodate/combodate.js")%>"></script>

  <!-- select2 -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/select2/select2.min.js"  )%>"></script>
  <!-- wysiwyg -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/jquery.hotkeys.js"  )%>"></script>
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/bootstrap-wysiwyg.js"  )%>"></script>
  <%--<script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/wysiwyg/demo.js"  )%>"></script>--%>
  <!-- markdown -->
  <script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/markdown/epiceditor.min.js"  )%>"></script>
  <%--<script src="<%  = Page.ResolveClientUrl("~/App_Theme_Responsive/js/markdown/demo.js"  )%>"></script>--%>



</body>
</html>




