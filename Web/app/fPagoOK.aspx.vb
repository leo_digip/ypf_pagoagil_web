﻿Imports YPF_Mobile.ServiceReference1

Public Class fPagoOK
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim r As New PreautorizacionPagoResult
        r = CType(Session("pre_result"), PreautorizacionPagoResult)




        If r.success Then

            Dim mont As String
            If r.monto = "TL" Then
                mont = "1000"
            Else
                mont = r.monto.Substring(0, r.monto.Length - 2)
            End If



            ltl_resultado.Text = "<h2 class='text-center'>Carga Aprobada</h2><div class='center-block text-center'><i class='fa fa-check-circle fa-4x'></i></div>"
            ltl_operacion.Text = "<h3 class='text-center'>Su Nº de operacion es: </br><h1>" & r.idTransaccion & "</br><p>Monto: $" & mont & "</p></br></h1></h3><p>Indique este numero al playero</p>"

        Else
            ltl_resultado.Text = "<h2 class='text-center'>Pago NO realizado</h2><div class='center-block text-center'><i class='fa text-danger fa-minus-circle fa-4x'></i></div>"
            ltl_message.Text = "<h3 class='text-center'>Mensaje: " & r.message & "</h3>"
        End If

        Session("monto") = Nothing




    End Sub

End Class