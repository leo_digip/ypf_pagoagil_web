﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fPrecios.aspx.vb" Inherits="YPF_Mobile.fPrecios1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contenido" runat="server">

    <section id="content">

        <section class="vbox">
            <section class="scrollable wrapper"  >
                <div class="col-xs-12 no-padder">
                        <section class="pan el  animated fadeInUpBig hidden">
	                        <div class="panel-body  bg-primary dker ">
		                        <div class="clearfix m-b col-sm-12 no-padder">
                                    <div class="col-sm-1 no-padder">
			                            <span class="pull-left">
                                            <i class="fa fa-road text-warning fa-3x m-t-xs"></i> 
			                            </span>
                                    </div>
			                        <div class="clear col-sm-10 no-padder">
                                        <h1 class="m-b-none m-t-sm"><strong>YPF Mobile</strong></h1>

                                        <small class="block h3 text-muted">Bienvenido</small> 
                                        <h5>
                                            <label class="badge bg-primary dk"><asp:Literal ID="ltl_usuario" runat="server"></asp:Literal></label>
                                            

                                        </h5>
			                        </div>
		                        </div>
		                
	                        </div>
	                        <footer class="panel-footer pos-rlt"> <span class="arrow top"></span>
		                        <div class="pull-out">
			                        <p class="h6 m-l m-t">
                                        Si necesitás ayuda podés contactarnos por estos medios<br />
                                        Email: <a href="mailto:mesadeayuda@digip.com.ar">Mesadeayuda@digip.com.ar</a><br />
                                        Telefono: 011.5199.5725
                                    </p>
                                    <h4>
                                        <asp:Literal ID="lt_usuario_agente2" runat="server"></asp:Literal>
                                    </h4>
		                        </div>
	                        </footer>
                        </section>

                <div class=" animated fadeInUpBig ">
                    <div class="col-md -7 no-padder no-borders">
                        <section class="pan el bg-primary dker no-borders">
                            <div class="panel-heading dker h4 no-borders">Seleccionar cantidad a cargar</div>
                                <div class="panel-body no-borders">

                                  
                                    <button runat="Server" id="btn_100" class="col-xs-5 btn btn-primary m-b">
                                        <p class="font-semibold">
                                            <article class="media">
                                                <span class="h1">$100</span>
                                            </article>
                                        </p>
                                    </button>



                                    <div class="col-xs-2 m-b"></div>
                                    <button runat="Server" id="btn_200" class="col-xs-5 btn btn-primary m-b">
                                        <p class="font-semibold">
                                            <article class="media">
                                                <span class="h1">$200</span>
                                            </article>
                                        </p>
                                    </button>
                                    
                                    <button runat="Server" id="btn_300" class="col-xs-5  btn btn-primary">
                                        <p class="font-semibold">
                                            <article class="media">
                                                <span class="h1">$300</span>
                                            </article>
                                        </p>
                                    </button>
                                    <div class="col-xs-2 "></div>
                                    
                                    <button runat="Server" id="btn_400" class="col-xs-5  btn btn-primary">
                                        <p class="font-semibold">
                                            <article class="media">
                                                <span class="h1">$400</span>
                                            </article>
                                        </p>
                                    </button>
                                    <div></div>
                                    <button runat="Server" id="btn_lleno" class="col-xs-12  btn btn-primary m-t">
                                        <p class="font-semibold">
                                            <article class="media">
                                                <span class="h1">Tanque lleno</span>
                                            </article>
                                        </p>
                                    </button>

                                </div>
                        </section>
                    </div>
                         
                        
                        </div>
                        </div>
            </section>

        </section>
    </section>
    

</asp:Content>
