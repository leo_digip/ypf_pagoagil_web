﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Usuario.Master" CodeBehind="fCarga.aspx.vb" Inherits="YPF_Mobile.fPrecios" %>



<asp:Content ID="F1" ContentPlaceHolderID="Contenido" runat="server">

    <section id="content">

        <section class="vbox">
            <section class="scrollable wrapper"  >
                <div class="col-xs-12 no-padder">
                        <section class="pan el  animated fadeInUpBig hidden">
	                        <div class="panel-body  bg-primary dker ">
		                        <div class="clearfix m-b col-sm-12 no-padder">
                                    <div class="col-sm-1 no-padder">
			                            <span class="pull-left">
                                            <i class="fa fa-road text-warning fa-3x m-t-xs"></i> 
			                            </span>
                                    </div>
			                        <div class="clear col-sm-10 no-padder">
                                        <h1 class="m-b-none m-t-sm"><strong>YPF Mobile</strong></h1>

                                        <small class="block h3 text-muted">Bienvenido</small> 
                                        <h5>
                                            <label class="badge bg-primary dk"><asp:Literal ID="ltl_usuario" runat="server"></asp:Literal></label>
                                            

                                        </h5>
			                        </div>
		                        </div>
		                
	                        </div>
	                        <footer class="panel-footer pos-rlt"> <span class="arrow top"></span>
		                        <div class="pull-out">
			                        <p class="h6 m-l m-t">
                                        Si necesitás ayuda podés contactarnos por estos medios<br />
                                        Email: <a href="mailto:mesadeayuda@digip.com.ar">Mesadeayuda@digip.com.ar</a><br />
                                        Telefono: 011.5199.5725
                                    </p>
                                    <h4>
                                        <asp:Literal ID="lt_usuario_agente2" runat="server"></asp:Literal>
                                    </h4>
		                        </div>
	                        </footer>
                        </section>

                <div class=" animated fadeInUpBig ">
                    <div class="col-md -7 no-padder no-borders">
                        <section class="pan el bg-primary dker no-borders">
                            <div class="panel-heading dker h4 no-borders">Seleccionar combustible</div>
                                <div class="panel-body no-borders">

                                    <a href="/app/fPrecios.aspx"  class="font-semibold">
                                        <article class="media">
                                            <div class="pull-left wrapper">
                                                <span class="h1"><i class="fa fa-tint text-info"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <h5>INFINIA</h5>
                                                <div class="text-xs block m-t-xs">
                                                    <p>
                                                        <strong>El combustible inteligente de YPF.</strong>
                                                        <br />
                                                        Nafta de grado 3 diseñada para lograr el máximo desempeño, un excelente poder de limpieza y un óptimo rendimiento.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </a>

                                    <div class="line line-dashed"></div> 

                                    <a href="/app/fPrecios.aspx"  class="font-semibold">
                                        <article class="media">
                                            <div class="pull-left wrapper">
                                                <span class="h1"><i class="fa fa-tint text-primary"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <h5>NAFTA SUPER</h5>
                                                <div class="text-xs block m-t-xs">
                                                    <p>
                                                        Nafta de grado 2 formulada especialmente para brindar la máxima respuesta a la exigencia del motor, ayudando a alcanzar su mejor performance y con un adecuado control de las emisiones hacia el medio ambiente. 
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </a>

                                    <div class="line line-dashed"></div> 

                                    <a href="/app/fPrecios.aspx"  class="font-semibold">
                                        <article class="media">
                                            <div class="pull-left wrapper">
                                                <span class="h1"><i class="fa fa-tint text-warning "></i></span>
                                            </div>
                                            <div class="media-body">
                                                <h5>EURODIESEL</h5>
                                                <div class="text-xs block m-t-xs">
                                                    <p>
                                                        Combustible dirigido a motorizaciones Diesel que requieran la utilización de un gasoil grado 3 para su adecuado funcionamiento. 
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </a>

                                    <div class="line line-dashed"></div> 

                                    <a href="/app/fPrecios.aspx"  class="font-semibold">
                                        <article class="media">
                                            <div class="pull-left wrapper">
                                                <span class="h1"><i class="fa fa-tint text-white"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <h5>ULTRADIESEL</h5>
                                                <div class="text-xs block m-t-xs">
                                                    <p>
                                                        Combustible dirigido a motorizaciones Diesel que requieran la utilización de un gasoil grado 2 para su adecuado funcionamiento. 
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </a>

                                    <div class="line line-dashed"></div> 

                                    <a href="/app/fPrecios.aspx"  class="font-semibold">
                                        <article class="media">
                                            <div class="pull-left wrapper">
                                                <span class="h1"><i class="fa fa-tint text-muted"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <h5>DIESEL 500</h5>
                                                <div class="text-xs block m-t-xs">
                                                    <p>
                                                        La formulación de este producto está en concordancia con lo requerido para las motorizaciones EURO II y EURO III, incorporando en la misma un adecuado porcentaje de biocombustible (biodiésel)
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </a>
                                      


                                </div>
                        </section>
                    </div>
                         
                        
                        </div>
                        </div>
            </section>

        </section>
    </section>
    

</asp:Content>
