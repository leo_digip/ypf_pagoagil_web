﻿Public Class fRegistracion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dt_fechanac.Text = Now.ToString("yyyy-MM-dd")
            txt_password.Attributes("type") = "password"
            txt_password2.Attributes("type") = "password"

            cbo_provincia.Items.Add("Buenos Aires")
            cbo_provincia.Items.Add("Catamarca")
            cbo_provincia.Items.Add("Ciudad Autónoma de Buenos Aires")
            cbo_provincia.Items.Add("Chaco")
            cbo_provincia.Items.Add("Chubut")
            cbo_provincia.Items.Add("Córdoba")
            cbo_provincia.Items.Add("Corrientes")
            cbo_provincia.Items.Add("Entre Ríos")
            cbo_provincia.Items.Add("Formosa")
            cbo_provincia.Items.Add("Jujuy")
            cbo_provincia.Items.Add("La Pampa")
            cbo_provincia.Items.Add("La Rioja")
            cbo_provincia.Items.Add("Mendoza")
            cbo_provincia.Items.Add("Misiones")
            cbo_provincia.Items.Add("Neuquén")
            cbo_provincia.Items.Add("Río Negro")
            cbo_provincia.Items.Add("Salta")
            cbo_provincia.Items.Add("San Juan")
            cbo_provincia.Items.Add("San Luis")
            cbo_provincia.Items.Add("Santa Cruz")
            cbo_provincia.Items.Add("Santa Fe")
            cbo_provincia.Items.Add("Santiago del Estero")
            cbo_provincia.Items.Add("Tierra del Fuego, Antártida e Islas del Atlántico Sur")
            cbo_provincia.Items.Add("Tucumán")

            'cbo_provincia.Items.Add(" ")
            'cbo_provincia.SelectedValue = " "
        End If
    End Sub

    Protected Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click






        Dim tu As New NEGOCIO.tUsuarios
        Dim tt As New NEGOCIO.tTags
        Dim cu As New NEGOCIO.cUsuarios
        Dim ct As New NEGOCIO.cTags

        Dim rt As New cGlobales.respuesta
        Dim ru As New cGlobales.respuesta

        tu.ACTIVO = True
        tu.ALTURA = txt_altura.Text
        tu.APELLIDO = txt_apellido.Text
        tu.CALLE = txt_calle.Text
        tu.DOC = txt_doc.Text
        tu.EMAIL = txt_email.Text
        tu.FECHA_NACIMIENTO = dt_fechanac.Text
        tu.FECHAALTA = Date.Now
        tu.ID_ROL = 2
        tu.LOCALIDAD = txt_localidad.Text
        tu.NOMBRE = txt_nombre.Text
        tu.NYACOMPLETO = txt_apellido.Text & " " & txt_apellido.Text
        tu.PASSWORD = txt_password.Text
        tu.PROVINCIA = cbo_provincia.SelectedValue
        tu.TELEFONO = txt_cel.Text

        ru = cu.addObjeto(tu)

        tt.activo = True
        tt.doc = txt_doc.Text
        tt.fechaalta = Date.Now
        tt.pin = txt_pin.Text
        tt.verificador = txt_verificador.Text
        tt.tarjeta = txt_tarjeta.Text
        'tt.tagid

        rt = ct.addObjeto(tt)

        If rt.status = True And ru.status = True Then
            'modificacion exitosa
            Server.TransferRequest("/login.aspx", False)
        Else
            'error
            Stop
        End If


    End Sub



End Class