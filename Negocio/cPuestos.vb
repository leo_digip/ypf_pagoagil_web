﻿Public Class cPuestos
    Public Function getList() As List(Of NEGOCIO.tPuestos)
        Dim k As New dbNFC
        Dim t As New List(Of tPuestos)
        t = (From a In k.tPuestos).ToList
        Return t
    End Function

    Function addObjeto(p As tPuestos) As cGlobales.respuesta
        Dim r As New cGlobales.respuesta
        Try
            Dim k As New dbNFC
            k.tPuestos.Add(p)
            k.SaveChanges()
            r.status = True
            Return r
        Catch ex As Exception
            r.status = True
            r.msg = ex.Message
            Return r
        End Try
    End Function

    Function GetObjeto(id As Integer) As tPuestos
        Dim k As New dbNFC
        Return (From a In k.tPuestos Where a.id = id).FirstOrDefault
    End Function

    Function SetObjeto(ent As tPuestos) As cGlobales.respuesta
        Dim k As New dbNFC
        Dim r As New cGlobales.respuesta
        Dim t As tPuestos = (From a In k.tPuestos Where a.id = ent.id).FirstOrDefault
        t.descripcion = ent.descripcion
        t.activo = ent.activo
        Dim ss As Integer
        ss = k.SaveChanges
        If ss = 1 Then
            r.msg = "Elemento guardado exitosamente."
            r.status = True
        Else
            r.msg = "Actualizados " & ss & " registros."
            r.status = False
        End If
        Return r

    End Function

End Class
