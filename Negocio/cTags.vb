﻿Public Class cTags
    Public Function getList() As List(Of NEGOCIO.ttags)
        Dim k As New dbNFC
        Dim t As New List(Of ttags)
        t = (From a In k.ttags).ToList
        Return t
    End Function

    Function addObjeto(p As ttags) As cGlobales.respuesta
        Dim r As New cGlobales.respuesta
        Try
            Dim k As New dbNFC
            k.ttags.Add(p)
            k.SaveChanges()
            r.status = True
            Return r
        Catch ex As Exception
            r.status = True
            r.msg = ex.Message
            Return r
        End Try
    End Function

    Function GetObjeto(id As String) As tTags
        Dim k As New dbNFC
        Return (From a In k.tTags Where a.tagid = id).FirstOrDefault
    End Function
     
    Function SetObjeto(ent As ttags) As cGlobales.respuesta
        Dim k As New dbNFC
        Dim r As New cGlobales.respuesta
        Dim t As ttags = (From a In k.ttags Where a.tagid = ent.tagid).FirstOrDefault
        t.descripcion = ent.descripcion
        t.pin = ent.pin
        t.verificador = ent.verificador
        t.tarjeta = ent.tarjeta
        t.doc = ent.doc
        t.fechamodif = Date.Now
        t.latitud = ent.latitud
        t.longitud = ent.longitud
        t.activo = ent.activo
        If t.activo = False Then
            t.fechabaja = Date.Now
        End If
        Dim ss As Integer
        ss = k.SaveChanges
        If ss = 1 Then
            r.msg = "Elemento guardado exitosamente."
            r.status = True
        Else
            r.msg = "Actualizados " & ss & " registros."
            r.status = False
        End If
        Return r
    End Function

End Class
