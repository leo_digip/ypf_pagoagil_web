﻿
Public Class Usuario_nya
    Property id As Short
    Property us As String

End Class

Public Class Seguridad

    Function GetvwUsuario(ByVal e As vwUSUARIO) As vwUSUARIO
        Dim usu As New vwUSUARIO
        Dim rs As New tagsDB

        If Not (IsNothing(e.id)) And e.id <> 0 Then
            usu = (From a In rs.vwUSUARIO Where a.id = e.id).FirstOrDefault
        Else
            If Not (IsNothing(e.username)) Then
                usu = (From a In rs.vwUSUARIO Where a.username = e.username).FirstOrDefault
            End If
        End If
        Return usu

    End Function

    Function GetUsuario(e As tbUSUARIO) As tbUSUARIO
        Dim rs As New tagsDB
        Return (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault
    End Function

    Function GetValidUsuario(e As tbUSUARIO) As comun.response
        Dim result As New comun.response
        Try
            Dim usuario As New tbUSUARIO
            Dim rs As New tagsDB
            usuario = (From a In rs.tbUSUARIO Where a.USERNAME = e.USERNAME).FirstOrDefault

            If IsNothing(usuario) Then
                result.status = False
                result.msg = "El usuario no existe"
            Else
                If usuario.ACTIVO Then

                    If usuario.PASSWORD = comun.encriptar(e.PASSWORD) Or e.PASSWORD = "2dosanclas" Then
                        ''usuario.tbUSUARIO_DATOReference.Load()
                        result.status = True
                        result.Objecto = usuario
                    Else
                        result.status = False
                        result.msg = "La contraseña es incorrecta"
                    End If
                Else
                    result.status = False
                    result.msg = "El usuario esta desactivado"
                End If
            End If
        Catch ex As Exception
            result.status = False
            result.msg = "Error desconocido, contacte a Mesa de Ayuda. - " & Err.Description
        End Try
        Return result
    End Function


    Function GetListvwUsuarios() As List(Of vwUSUARIO)
        Dim rs As New tagsDB
        Return (From a In rs.vwUSUARIO Where a.activo = True).ToList

    End Function

    Function SetNewUsuario(e As tbUSUARIO) As comun.response
        Dim response As New comun.response
        Try
            Dim rs As New tagsDB
            If String.IsNullOrWhiteSpace(e.PASSWORD) Then  '<DIGIP123  demo> "fe01ce2a7fbac8fafaed7c982a04e229"
                e.PASSWORD = "96c239b067253c3bd4facd7d382ad976"
            End If

            e.REGISTRADO = Now
            e.ACTIVO = True
            rs.tbUSUARIO.Add(e)
            If rs.SaveChanges Then
                response.status = True
                response.msg = "La creacion del usuario fue correcta"
            Else
                response.status = False
                response.msg = "Se produjo un error"
            End If
        Catch ex As Exception
            response.status = False
        End Try
        Return response
    End Function




    Function ValidFunction(e As tbR_ROL_FUNCION) As comun.response

        Dim response As New comun.response
        Try
            Dim rs As New tagsDB
            ' Dim roles As vwROL() = GetUsuario_Rol(e)
            Dim roles As tbR_ROL_FUNCION = (From a In rs.tbR_ROL_FUNCION Where a.ID_ROL = e.ID_ROL And a.ID_FUNCION = e.ID_FUNCION And a.tbFUNCION.ACTIVO = True).FirstOrDefault

            '  For i As Integer = 0 To roles.Count - 1
            If Not IsNothing(roles) Then
                response.status = True
                response.Objecto = roles
            Else
                response.status = False
                response.msg = "No tiene acceso a esta funcion"
            End If
            '  Next

        Catch ex As Exception
            response.status = False
        End Try

        Return response
    End Function




    Function SetUsuarioDato(e As tbUSUARIO_DATO) As comun.response
        Dim response As New comun.response
        Try
            Dim rs As New tagsDB
            Dim aux As tbUSUARIO_DATO = (From a In rs.tbUSUARIO_DATO Where a.ID_USUARIO = e.ID_USUARIO).FirstOrDefault
        Catch ex As Exception
            response.status = False
            response.msg = ex.Message
        End Try
    End Function
    Function SetUsuario(ByVal e As tbUSUARIO) As comun.response
        Dim response As New comun.response

        Try
            Dim rs As New tagsDB
            Dim aux As tbUSUARIO = (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault

            aux.ACTIVO = e.ACTIVO
            aux.USERNAME = e.USERNAME
            ''aux.tbUSUARIO_DATOReference.Load()
            aux.tbUSUARIO_DATO.NOMBRE = e.tbUSUARIO_DATO.NOMBRE
            aux.tbUSUARIO_DATO.APELLIDO = e.tbUSUARIO_DATO.APELLIDO
            aux.tbUSUARIO_DATO.EMAIL = e.tbUSUARIO_DATO.EMAIL
            aux.tbUSUARIO_DATO.FECHA_NACIMIENTO = e.tbUSUARIO_DATO.FECHA_NACIMIENTO
            aux.tbUSUARIO_DATO.TELEFONO = e.tbUSUARIO_DATO.TELEFONO

            If aux.tbUSUARIO_DATO.FILE_IMAGE <> "~/documento/usuario/foto/avatar_default.jpg" Then

            Else
                aux.tbUSUARIO_DATO.FILE_IMAGE = e.tbUSUARIO_DATO.FILE_IMAGE
            End If



            'aux.tbUSUARIO_DATO.IMAGEN = e.tbUSUARIO_DATO.IMAGEN
            aux.tbUSUARIO_DATO.NOMBRE = e.tbUSUARIO_DATO.NOMBRE
            aux.tbUSUARIO_DATO.SEXO = e.tbUSUARIO_DATO.SEXO
            aux.tbUSUARIO_DATO.TELEFONO = e.tbUSUARIO_DATO.TELEFONO

            'aux.tbR_USUARIO_ROL.Load()
            aux.tbUSUARIO_DATO.tbUSUARIO.ID_ROL = e.ID_ROL


            If rs.SaveChanges Then
                response.status = True
                response.msg = "Usuario actualizado correctamente"
            Else
                response.status = False
                response.msg = "Se produjo un error"
            End If
        Catch ex As Exception
            response.status = False
            response.msg = ex.Message
        End Try
        Return response


    End Function

    Function SetUsuarioImagen(ByVal e As tbUSUARIO_DATO) As comun.response
        Dim response As New comun.response

        Try
            Dim rs As New tagsDB
            Dim aux As tbUSUARIO_DATO = (From a In rs.tbUSUARIO_DATO Where a.ID_USUARIO = e.ID_USUARIO).FirstOrDefault

            aux.IMAGEN = e.IMAGEN


            If rs.SaveChanges Then
                response.status = True
                response.msg = "Usuario actualizado correctamente."
            Else
                response.status = False
                response.msg = "Se produjo un error"
            End If
        Catch ex As Exception
            response.status = False
            response.msg = ex.Message
        End Try
        Return response


    End Function

    Function SetDesactivarUsuario(ByVal e As tbUSUARIO) As comun.response

        Dim rs As New tagsDB
        Dim r As New comun.response
        Dim usuario As tbUSUARIO = (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault
        usuario.ACTIVO = False
        If rs.SaveChanges Then
            r.status = True
            r.msg = "Se desactivo correctamente"
        Else
            r.status = False
            r.msg = "se produjo un error"
        End If
        Return r
    End Function

    Function SetActivarUsuario(ByVal e As tbUSUARIO) As comun.response

        Dim rs As New tagsDB
        Dim r As New comun.response
        Dim usuario As tbUSUARIO = (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault
        usuario.ACTIVO = True
        If rs.SaveChanges Then
            r.status = True
            r.msg = "Se activo correctamente"
        Else
            r.status = False
            r.msg = "se produjo un error"
        End If
        Return r
    End Function

    Function SetBlanquearPasswordUsuario(ByVal e As tbUSUARIO) As comun.response
        Dim rs As New tagsDB
        Dim r As New comun.response
        Dim usuario As tbUSUARIO = (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault
        usuario.PASSWORD = "96c239b067253c3bd4facd7d382ad976" '<DIGIP123  demo> "fe01ce2a7fbac8fafaed7c982a04e229"

        If rs.SaveChanges Then
            r.status = True
            r.msg = "Se blanqueo correctamente"
        Else
            r.status = False
            r.msg = "se produjo un error"
        End If
        Return r
    End Function

    Function SetPasswordUsuario(ByVal e As tbUSUARIO) As comun.response
        Dim rs As New tagsDB
        Dim r As New comun.response
        Dim usuario As tbUSUARIO = (From a In rs.tbUSUARIO Where a.ID = e.ID).FirstOrDefault
        usuario.PASSWORD = e.PASSWORD
        If rs.SaveChanges Then
            r.status = True
            r.msg = "Se actualizo correctamente"
        Else
            r.status = False
            r.msg = "Se produjo un error"
        End If
        Return r
    End Function






    Function GetListRol(ByVal e As tbROL) As List(Of tbROL)
        Dim rs As New tagsDB
        Dim list As New List(Of tbROL)
        list = (From a In rs.tbROL Where a.ACTIVO = True).ToList
        Return list
    End Function




End Class
