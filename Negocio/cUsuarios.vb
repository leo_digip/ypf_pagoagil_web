﻿Public Class cUsuarios
    Public Function getList() As List(Of NEGOCIO.tusuarios)
        Dim k As New dbNFC
        Dim t As New List(Of tusuarios)
        t = (From a In k.tusuarios).ToList
        Return t
    End Function

    Function addObjeto(p As tusuarios) As cGlobales.respuesta
        Dim r As New cGlobales.respuesta
        Try
            Dim k As New dbNFC
            k.tusuarios.Add(p)
            k.SaveChanges()
            r.status = True
            Return r
        Catch ex As Exception
            r.status = True
            r.msg = ex.Message
            Return r
        End Try
    End Function

    Function GetObjeto(id As Integer) As tusuarios
        Dim k As New dbNFC
        Return (From a In k.tusuarios Where a.id = id).FirstOrDefault
    End Function
     
    Function GetUsuario(doc As Integer) As tUsuarios
        Dim k As New dbNFC
        Return (From a In k.tUsuarios Where a.DOC = doc).FirstOrDefault
    End Function


    Function SetObjeto(ent As tusuarios) As cGlobales.respuesta
        Try

        
        Dim k As New dbNFC
        Dim r As New cGlobales.respuesta
        Dim t As tusuarios = (From a In k.tusuarios Where a.id = ent.id).FirstOrDefault
        t.PASSWORD = ent.PASSWORD
        t.ID_ROL = ent.ID_ROL
        t.ACTIVO = ent.ACTIVO
        t.FECHAULTMODIF = Date.Now
        t.APELLIDO = ent.APELLIDO
        t.NOMBRE = ent.NOMBRE
        t.TELEFONO = ent.TELEFONO
        t.SEXO = ent.SEXO
        t.FECHA_NACIMIENTO = ent.FECHA_NACIMIENTO
        t.EMAIL = ent.EMAIL
        t.IMAGEN = ent.IMAGEN
        t.FILE_IMAGE = ent.FILE_IMAGE
        t.NYACOMPLETO = ent.NYACOMPLETO
        t.CALLE = ent.CALLE
        t.ALTURA = ent.ALTURA
        t.LOCALIDAD = ent.LOCALIDAD
        t.PROVINCIA = ent.PROVINCIA
        t.DOC = ent.DOC

         

        Dim ss As Integer
        ss = k.SaveChanges
        If ss = 1 Then
            r.msg = "Elemento guardado exitosamente."
            r.status = True
        Else
            r.msg = "Actualizados " & ss & " registros."
            r.status = False
        End If
            Return r



        Catch ex As Exception
            Stop
        End Try

    End Function


End Class
