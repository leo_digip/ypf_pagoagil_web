﻿
Public Class Resum
    Public Property colu As String
    Public Property tot As Integer
End Class

Public Class cInformes

    Function CerrarTurno(id As Long) As Boolean
        Dim k As New dbNFC
        Dim t As tMovimientos
        t = (From a In k.tMovimientos Where a.id = id).FirstOrDefault
        t.salida = Date.Now
        t.cerrado = True

        Dim ss As Integer
        ss = k.SaveChanges
        If ss = 1 Then
            Return True
        Else
            Return False
        End If

    End Function

    Function getvwMovimientos2(desde As String, hasta As String, doc As String, puesto As String, abiertos As Boolean) As List(Of vwMovimientos)
        Dim k As New dbNFC
        Dim where As String = " where entrada>='" & CDate(desde).ToString("dd/MM/yyyy") & "' And entrada<='" & CDate(hasta).ToString("dd/MM/yyyy") & " 23:59:59" & "'"
        
        If doc <> "Todos" Then
            where = where & " and doc=" & doc
        End If
        If puesto <> "Todos" Then
            where = where & " and idpuesto=" & puesto
        End If
        If abiertos = True Then
            where = where & " and cerrado = 0 "
        End If
        Dim sql As String = "select * from vwMovimientos " & where
        Return k.Database.SqlQuery(Of vwMovimientos)(sql).ToList
    End Function



    Function getvwMovimientos(desde As String, hasta As String, tagid As String, doc As String, puesto As String, abiertos As Boolean) As List(Of vwMovimientos)
        Dim k As New dbNFC
        Dim where As String = " where entrada>='" & CDate(desde).ToString("dd/MM/yyyy") & "' And entrada<='" & CDate(hasta).ToString("dd/MM/yyyy") & " 23:59:59" & "'"
        If tagid <> "Todos" Then
            where = where & " and tagid='" & tagid & "'"
        End If
        If doc <> "Todos" Then
            where = where & " and doc=" & doc
        End If
        If puesto <> "Todos" Then
            where = where & " and idpuesto=" & puesto
        End If
        If abiertos = True Then
            where = where & " and cerrado = 0 "
        End If
        Dim sql As String = "select * from vwMovimientos " & where
        Return k.Database.SqlQuery(Of vwMovimientos)(sql).ToList
    End Function

    Function getvwMovimientosUsuario(desde As String, hasta As String, doc As String) As List(Of vwMovimientos)
        Dim k As New dbNFC
        Dim where As String = " where entrada>='" & CDate(desde).ToString("dd/MM/yyyy") & "' And entrada<='" & CDate(hasta).ToString("dd/MM/yyyy") & " 23:59:59" & "'"
        where = where & " and doc=" & doc
        'If abiertos = True Then
        ' where = where & " and cerrado = 0 "
        'End If
        Dim sql As String = "select * from vwMovimientos " & where
        Return k.Database.SqlQuery(Of vwMovimientos)(sql).ToList
    End Function



    Function getvwMovimientosUsuarioResumen(desde As String, hasta As String, doc As String) As List(Of Resum)
        Dim k As New dbNFC
        Dim where As String = " where entrada>='" & CDate(desde).ToString("dd/MM/yyyy") & "' And entrada<='" & CDate(hasta).ToString("dd/MM/yyyy") & " 23:59:59" & "'"
        where = where & " and doc=" & doc
        'If abiertos = True Then
        ' where = where & " and cerrado = 0 "
        'End If
        Dim sql1 As String = " select 'Visitas' as colu, count(*) as tot from vwMovimientos " & where
        Dim sql2 As String = " union all (select 'Promedio' as colu, avg(minu) as tot from vwMovimientos " & where & ")"
        Dim sql3 As String = " union all (select 'Total' as colu, sum(minu) as tot from vwMovimientos " & where & ")"

        Dim sql As String = sql1 & sql2 & sql3
        Return k.Database.SqlQuery(Of Resum)(sql).ToList
    End Function




    'Function getvwUltimaUbicacion(idproveedor As String, idestado As String, idinstalador As String, idusuario As String) As List(Of vwUltimaUbicacion)
    '    Dim k As New dbNFC
    '    Dim where As String = " where 1=1 "

    '    If idproveedor <> "Todos" Then
    '        where = where & " and idproveedor='" & idproveedor & "'"
    '    End If
    '    If idestado <> "Todos" Then
    '        where = where & " and idestado=" & idestado
    '    End If
    '    If idinstalador <> "Todos" Then
    '        where = where & " and idinstalador =" & idinstalador
    '    End If
    '    If idusuario <> "Todos" Then
    '        where = where & " and idusuario =" & idusuario
    '    End If


    '    Dim sql As String = "select * from vwUltimaUbicacion " & where
    '    Return k.Database.SqlQuery(Of vwUltimaUbicacion)(sql).ToList
    'End Function



End Class
