﻿Public Class cRoles
    Public Function getList() As List(Of NEGOCIO.tRoles)
        Dim k As New dbNFC
        Dim t As New List(Of tRoles)
        t = (From a In k.tRoles).ToList
        Return t
    End Function

    Function addObjeto(p As tRoles) As cGlobales.respuesta
        Dim r As New cGlobales.respuesta
        Try
            Dim k As New dbNFC
            k.tRoles.Add(p)
            k.SaveChanges()
            r.status = True
            Return r
        Catch ex As Exception
            r.status = True
            r.msg = ex.Message
            Return r
        End Try
    End Function

    Function GetObjeto(id As Integer) As tRoles
        Dim k As New dbNFC
        Return (From a In k.tRoles Where a.ID = id).FirstOrDefault
    End Function

    Function SetObjeto(ent As tRoles) As cGlobales.respuesta
        Dim k As New dbNFC
        Dim r As New cGlobales.respuesta
        Dim t As tRoles = (From a In k.tRoles Where a.ID = ent.ID).FirstOrDefault
        t.DESCRIPCION = ent.DESCRIPCION
        t.ULT_MODIFICACION = Date.Now
        t.ACTIVO = ent.ACTIVO
        Dim ss As Integer
        ss = k.SaveChanges
        If ss = 1 Then
            r.msg = "Elemento guardado exitosamente."
            r.status = True
        Else
            r.msg = "Actualizados " & ss & " registros."
            r.status = False
        End If
        Return r
    End Function

End Class
