﻿Imports System.Data.SqlClient
Imports NEGOCIO.RTLSModel

Public Class Empresa
    Public Function dt_select_busca_empresa(ByVal empresa As tbEMPRESA) As DataTable
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim dt As New DataTable
        Dim adapter As New SqlDataAdapter

        sql = "select razon_social, cuit, activo, fec_ult_act, ult_modificacion, telefono, email, web "
        sql = sql & "from  tbEMPRESA "

        If Not IsNothing(empresa) Then
            If empresa.ID <> 0 Then
                sql = sql & " where id = @id"
            ElseIf Not IsNothing(empresa.RAZON_SOCIAL) Then
                sql = sql & " where  razon_social like @razon_social "
            End If
        End If
        sql = sql & " order by razon_social asc"
        cmd = New SqlCommand(sql, cnn)
        If Not IsNothing(empresa) Then
            cmd.Parameters.AddWithValue("id", empresa.ID)
            cmd.Parameters.AddWithValue("razon_social", "%" & empresa.RAZON_SOCIAL & "%")
        End If
        cnn.Open()
        adapter.SelectCommand = cmd
        adapter.Fill(dt)
        Return dt
    End Function

    Public Function dt_select_empresa() As DataTable
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim dt As New DataTable
        Dim adapter As New SqlDataAdapter
        sql = "Select * from tbEMPRESA order by RAZON_SOCIAL asc"
        cmd = New SqlCommand(sql, cnn)
        cnn.Open()
        adapter.SelectCommand = cmd
        adapter.Fill(dt)
        Return dt
    End Function

    Public Function dt_select_empresa(ByVal entidad As tbEMPRESA) As DataTable
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim dt As New DataTable
        Dim adapter As New SqlDataAdapter
        sql = "Select * from tbEMPRESA where id = @id"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("id", entidad.ID)
        cnn.Open()
        adapter.SelectCommand = cmd
        adapter.Fill(dt)
        Return dt
    End Function


    Public Function select_empresa(ByVal entidad As tbEMPRESA) As tbEMPRESA
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim datos As New tbEMPRESA
        Dim reader As SqlDataReader
        sql = "Select * from tbEMPRESA where ID = @ID"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("ID", entidad.ID)
        cnn.Open()
        reader = cmd.ExecuteReader()
        If reader.HasRows Then

            reader.Read()
            datos.ID = reader("ID")
            datos.RAZON_SOCIAL = reader("RAZON_SOCIAL")
            datos.ACTIVO = reader("ACTIVO")
            datos.FEC_ULT_ACT = reader("FEC_ULT_ACT")
            datos.ULT_MODIFICACION = reader("ULT_MODIFICACION")
            datos.CUIT = reader("CUIT")
            datos.TELEFONO = reader("TELEFONO")
            datos.EMAIL = reader("EMAIL")
            datos.WEB = reader("WEB")
        End If
        cnn.Close()
        Return datos
    End Function

    Public Function insert_empresa(ByVal entidad As tbEMPRESA, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try

            sql = "Insert into tbEMPRESA (RAZON_SOCIAL, CUIT, ACTIVO, FEC_ULT_ACT, ULT_MODIFICACION, TELEFONO, EMAIL, WEB)"
            sql = sql & " Values (@RAZON_SOCIAL, @CUIT, @ACTIVO, getdate(), @ULT_MODIFICACION, @TELEFONO, @EMAIL, @WEB)"

            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If

            cmd.Parameters.AddWithValue("@RAZON_SOCIAL", entidad.RAZON_SOCIAL)
            cmd.Parameters.AddWithValue("@CUIT", entidad.CUIT)
            cmd.Parameters.AddWithValue("@ACTIVO", entidad.ACTIVO)
            cmd.Parameters.AddWithValue("@TELEFONO", entidad.TELEFONO)
            cmd.Parameters.AddWithValue("@ULT_MODIFICACION", entidad.ULT_MODIFICACION)
            cmd.Parameters.AddWithValue("@EMAIL", entidad.EMAIL)
            cmd.Parameters.AddWithValue("@WEB", entidad.WEB)

            If IsNothing(tran) Then
                cnn.Open()
                resultado.scope_identity = cmd.ExecuteScalar()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "El ingreso fue correcto"

        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
        Return resultado
    End Function

    Public Function update_empresa(ByVal Entidad As tbEMPRESA, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try
            sql = "update tbEMPRESA set RAZON_SOCIAL=@RAZON_SOCIAL,CUIT=@CUIT, ACTIVO=@ACTIVO"
            sql = sql & ",TELEFONO=@TELEFONO,FEC_ULT_ACT=getdate(), ULT_MODIFICACION=@ULT_MODIFICACION, EMAIL=@EMAIL, WEB=@WEB"
            sql = sql & " where id=@id"
            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If
            cmd.Parameters.AddWithValue("@ID", Entidad.ID)
            cmd.Parameters.AddWithValue("@RAZON_SOCIAL", Entidad.RAZON_SOCIAL)
            cmd.Parameters.AddWithValue("@CUIT", Entidad.CUIT)
            cmd.Parameters.AddWithValue("@ACTIVO", Entidad.ACTIVO)
            cmd.Parameters.AddWithValue("@TELEFONO", Entidad.TELEFONO)
            cmd.Parameters.AddWithValue("@ULT_MODIFICACION", Entidad.ULT_MODIFICACION)
            cmd.Parameters.AddWithValue("@EMAIL", Entidad.EMAIL)
            cmd.Parameters.AddWithValue("@WEB", Entidad.WEB)

            If IsNothing(tran) Then
                cnn.Open()
                cmd.ExecuteNonQuery()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "La modificacion fue correcta"
            Return resultado
        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
    End Function
End Class
