﻿Imports System.Data.SqlClient
Imports NEGOCIO.RTLSModel

Public Class Funcion

    Public Function dt_select_funcion(ByVal entidad As tbFUNCION) As DataTable
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim dt As New DataTable
        Dim adapter As New SqlDataAdapter
        sql = "Select * from tbFUNCION where id = @id"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("id", entidad.ID)
        cnn.Open()
        adapter.SelectCommand = cmd
        adapter.Fill(dt)
        Return dt
    End Function


    Public Function select_funcion(ByVal entidad As tbFUNCION) As tbFUNCION
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim datos As New tbFUNCION
        Dim reader As SqlDataReader
        sql = "Select * from tbFUNCION where ID = @ID"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("ID", entidad.ID)
        cnn.Open()
        reader = cmd.ExecuteReader()
        If reader.HasRows Then

            reader.Read()
            datos.ID = reader("ID")
            datos.ID_FUNCION_GRUPO = reader("id_funcion_grupo")
            datos.MENU = reader("menu")
            datos.TITULO = reader("titulo")
            datos.DESCRIPCION = reader("descripcion")
            'datos.ayuda = reader("ayuda")
            datos.PATH = reader("path")
            datos.URL = reader("url")
            'datos.imagen = reader("imagen")
            datos.VISIBLE = reader("visible")
            datos.ACTIVO = reader("activo")
            datos.ORDEN = reader("orden")
            datos.SQL = reader("sql")
            'datos.valida = reader("valida")
        End If
        cnn.Close()
        Return datos
    End Function

    Public Function insert_funcion(ByVal entidad As tbFUNCION, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try

            sql = "Insert into tbFUNCION (id_funcion_grupo, menu, titulo, descripcion, ayuda, path, url, imagen, visible, activo, orden, sql, valida)"
            sql = sql & " Values (@id_funcion_grupo, @menu, @titulo, @descripcion, @ayuda, @path, @url, @imagen, @visible, @activo, @orden, @sql, @valida)"

            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If

            cmd.Parameters.AddWithValue("@id_funcion_grupo", entidad.ID_FUNCION_GRUPO)
            cmd.Parameters.AddWithValue("@menu", entidad.MENU)
            cmd.Parameters.AddWithValue("@titulo", entidad.TITULO)
            cmd.Parameters.AddWithValue("@descripcion", entidad.DESCRIPCION)
            'cmd.Parameters.AddWithValue("@ayuda", entidad.ayuda)
            cmd.Parameters.AddWithValue("@path", entidad.PATH)
            cmd.Parameters.AddWithValue("@url", entidad.URL)
            'cmd.Parameters.AddWithValue("@imagen", entidad.imagen)
            cmd.Parameters.AddWithValue("@visible", entidad.VISIBLE)
            cmd.Parameters.AddWithValue("@activo", entidad.ACTIVO)
            cmd.Parameters.AddWithValue("@orden", entidad.ORDEN)
            cmd.Parameters.AddWithValue("@sql", entidad.SQL)
            'cmd.Parameters.AddWithValue("@valida", entidad.valida)

            If IsNothing(tran) Then
                cnn.Open()
                resultado.scope_identity = cmd.ExecuteScalar()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "El ingreso fue correcto"

        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
        Return resultado
    End Function

    Public Function update_funcion(ByVal Entidad As tbFUNCION, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try
            sql = "update tbFUNCION set id_funcion_grupo=@id_funcion_grupo,menu=@menu, titulo=@titulo"
            sql = sql & ",descripcion=@descripcion,ayuda=@ayuda, path=@path, url=@url, imagen=@imagen "
            sql = sql & ",visible=@visible,activo=@activo, orden=@orden, sql=@sql, valida=@valida "
            sql = sql & " where id=@id"
            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If
            cmd.Parameters.AddWithValue("@ID", Entidad.ID)
            cmd.Parameters.AddWithValue("@id_funcion_grupo", Entidad.ID_FUNCION_GRUPO)
            cmd.Parameters.AddWithValue("@menu", Entidad.MENU)
            cmd.Parameters.AddWithValue("@titulo", Entidad.TITULO)
            cmd.Parameters.AddWithValue("@descripcion", Entidad.DESCRIPCION)
            'cmd.Parameters.AddWithValue("@ayuda", Entidad.ayuda)
            cmd.Parameters.AddWithValue("@path", Entidad.PATH)
            cmd.Parameters.AddWithValue("@url", Entidad.URL)
            'cmd.Parameters.AddWithValue("@imagen", Entidad.imagen)
            cmd.Parameters.AddWithValue("@visible", Entidad.VISIBLE)
            cmd.Parameters.AddWithValue("@activo", Entidad.ACTIVO)
            cmd.Parameters.AddWithValue("@orden", Entidad.ORDEN)
            cmd.Parameters.AddWithValue("@sql", Entidad.SQL)
            'cmd.Parameters.AddWithValue("@valida", Entidad.valida)

            If IsNothing(tran) Then
                cnn.Open()
                cmd.ExecuteNonQuery()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "La modificacion fue correcta"
            Return resultado
        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
    End Function
End Class
