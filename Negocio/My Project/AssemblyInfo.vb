Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("Digip.Tags.Negocio")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Digip")> 
<Assembly: AssemblyProduct("NEGOCIO")> 
<Assembly: AssemblyCopyright("Copyright © Digip 2015")> 
<Assembly: AssemblyTrademark("Digip")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("9b73e220-57d5-4863-a1b9-36fa5dce8629")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.9.0.6000")> 
