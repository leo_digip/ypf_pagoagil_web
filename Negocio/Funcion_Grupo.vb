﻿Imports System.Data.SqlClient
Imports NEGOCIO.RTLSModel

Public Class Funcion_Grupo
    Public Function dt_select_funcion_grupo(ByVal entidad As tbFUNCION_GRUPO) As DataTable
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim dt As New DataTable
        Dim adapter As New SqlDataAdapter
        sql = "Select * from tbFUNCION_GRUPO where id = @id"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("id", entidad.ID)
        cnn.Open()
        adapter.SelectCommand = cmd
        adapter.Fill(dt)
        Return dt
    End Function


    Public Function select_funcion_grupo(ByVal entidad As tbFUNCION_GRUPO) As tbFUNCION_GRUPO
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Dim datos As New tbFUNCION_GRUPO
        Dim reader As SqlDataReader
        sql = "Select * from tbFUNCION_grupo where ID = @ID"
        cmd = New SqlCommand(sql, cnn)
        cmd.Parameters.AddWithValue("ID", entidad.ID)
        cnn.Open()
        reader = cmd.ExecuteReader()
        If reader.HasRows Then

            reader.Read()
            datos.ID = reader("ID")
            datos.DESCRIPCION_CORTA = reader("descripcion_corta")
            datos.DESCRIPCION_LARGA = reader("descripcion_larga")
            datos.URL = reader("url")
            datos.IMAGEN = reader("imagen")
            datos.VISIBLE = reader("visible")
            datos.ACTIVO = reader("activo")
        End If
        cnn.Close()
        Return datos
    End Function

    Public Function insert_funcion_grupo(ByVal entidad As tbFUNCION_GRUPO, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try

            sql = "Insert into tbFUNCION_GRUPO (descripcion_corta, descripcion_larga, url, imagen, visible, activo)"
            sql = sql & " Values (@descripcion_corta, @descripcion_larga, @url, @imagen, @visible, @activo)"

            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If

            cmd.Parameters.AddWithValue("@descripcion_corta", entidad.DESCRIPCION_CORTA)
            cmd.Parameters.AddWithValue("@descripcion_larga", entidad.DESCRIPCION_LARGA)
            cmd.Parameters.AddWithValue("@url", entidad.URL)
            cmd.Parameters.AddWithValue("@imagen", entidad.IMAGEN)
            cmd.Parameters.AddWithValue("@visible", entidad.VISIBLE)
            cmd.Parameters.AddWithValue("@activo", entidad.ACTIVO)

            If IsNothing(tran) Then
                cnn.Open()
                resultado.scope_identity = cmd.ExecuteScalar()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "El ingreso fue correcto"

        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
        Return resultado
    End Function

    Public Function update_funcion_grupo(ByVal Entidad As tbFUNCION_GRUPO, Optional ByVal tran As SqlTransaction = Nothing) As comun.response
        Dim resultado As New comun.response
        Dim cnn As New SqlConnection(Database.conexion)
        Dim sql As String
        Dim cmd As SqlCommand
        Try
            sql = "update tbFUNCION_GRUPO set descripcion_corta=@descripcion_corta,descripcion_larga=@descripcion_larga, url=@url"
            sql = sql & ",imagen=@imagen,visible=@visible, activo=@activo "
            sql = sql & " where id=@id"
            If IsNothing(tran) Then
                cmd = New SqlCommand(sql, cnn)
            Else
                cmd = New SqlCommand(sql, tran.Connection, tran)
            End If
            cmd.Parameters.AddWithValue("@ID", Entidad.ID)
            cmd.Parameters.AddWithValue("@descripcion_corta", Entidad.DESCRIPCION_CORTA)
            cmd.Parameters.AddWithValue("@descripcion_larga", Entidad.DESCRIPCION_LARGA)
            cmd.Parameters.AddWithValue("@url", Entidad.URL)
            cmd.Parameters.AddWithValue("@imagen", Entidad.IMAGEN)
            cmd.Parameters.AddWithValue("@visible", Entidad.VISIBLE)
            cmd.Parameters.AddWithValue("@activo", Entidad.ACTIVO)

            If IsNothing(tran) Then
                cnn.Open()
                cmd.ExecuteNonQuery()
                cnn.Close()
            Else
                resultado.scope_identity = cmd.ExecuteScalar()
            End If
            resultado.status = True
            resultado.msg = "La modificacion fue correcta"
            Return resultado
        Catch ex As Exception
            cnn.Close()
            resultado.status = False
            resultado.msg = ex.Message
            Return resultado
        End Try
    End Function

End Class
