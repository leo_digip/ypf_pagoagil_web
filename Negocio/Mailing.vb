﻿Imports System.Net.Mail
Imports log4net


Public Module Mailing
    Public Sub Send(subject As String, body As String, recipients As String())
        Try
            Dim server = SettingsManager.GetValue("mail.server")
            Dim port = 587 ' SettingsManager.GetValue("mail.port")
            Dim ssl = True 'SettingsManager.GetValue("mail.ssl").ToUpper().Equals("TRUE")
            Dim user = SettingsManager.GetValue("mail.user")
            Dim pwd = SettingsManager.Decrypt(SettingsManager.GetValue("mail.pwd"))
            Dim mailFrom = SettingsManager.GetValue("mail.from")


            'Dim server = "aspmx.l.google.com"
            'Dim port = "25"
            'Dim ssl = False ' SettingsManager.GetValue("mail.ssl").ToUpper().Equals("TRUE")
            'Dim user = "leo.digip@gmail.com"
            'Dim pwd = "brightstar2"
            'Dim mailFrom = "leo.digip@gmail.com"

            'Dim server = "mail.digip.com.ar"
            'Dim port = SettingsManager.GetValue("mail.port")
            'Dim ssl = SettingsManager.GetValue("mail.ssl").ToUpper().Equals("TRUE")
            'Dim user = "leo.digip@gmail.com"
            'Dim pwd = "brightstar2"
            'Dim mailFrom = "leo.digip@gmail.com"

            If String.IsNullOrEmpty(server) Or String.IsNullOrEmpty(port) Or String.IsNullOrEmpty(user) Or String.IsNullOrEmpty(pwd) Then
                Throw New ApplicationException("La funcionalidad de mail no se ha configurado.")
            End If


            Dim mail = New MailMessage()

            mail.From = New MailAddress(mailFrom)
            mail.To.Add(Join(recipients, ","))


            mail.Subject = subject

            mail.Body = body

            mail.IsBodyHtml = True
            Dim smtp = New SmtpClient()
            smtp.Host = server
            smtp.Port = port
            smtp.EnableSsl = ssl
            smtp.Credentials = New System.Net.NetworkCredential(user, pwd)
            smtp.Timeout = 60000
            smtp.Send(mail)



        Catch ex As SmtpException
            '     MsgBox(ex.Message)
            '     MsgBox(ex.InnerException.ToString)
            Dim Logger As ILog = LogManager.GetLogger(String.Format("Digip.Tags.Core.Interprete"))

            Logger.Error(vbCrLf & vbCrLf & "***Error al enviar correo *** " & vbCrLf & vbCrLf & ex.Message & vbCrLf & vbCrLf)

        End Try



    End Sub
End Module
