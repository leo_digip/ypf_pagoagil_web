﻿Imports System.Security.Cryptography

Public Class comun
    Public Structure response
        Public status As Boolean
        Public msg As String
        Public scope_identity As String
        Public rows As Int32
        Public Objecto As Object
    End Structure
    Public Shared Function encriptar(ByVal cadena As String)
        Dim md5 As MD5CryptoServiceProvider
        Dim bytValue() As Byte
        Dim bytHash() As Byte
        Dim strPassOutput As String
        Dim i As Integer
        strPassOutput = ""
        md5 = New MD5CryptoServiceProvider
        bytValue = System.Text.Encoding.UTF8.GetBytes(cadena)
        bytHash = md5.ComputeHash(bytValue)
        md5.Clear()
        For i = 0 To bytHash.Length - 1
            strPassOutput &= bytHash(i).ToString("x").PadLeft(2, "0")
        Next
        Return strPassOutput
    End Function
    Public Shared Function Sql_Execute(Sql As String) As DataTable
        Dim rs As New dbNFC

        Return rs.Database.SqlQuery(Of DataTable)(Sql)(0).DefaultView.ToTable
    End Function

    Public Shared Function isnull(objeto As Object, Optional refencias As Object = Nothing) As Object
        If IsNothing(objeto) Then
            Return DBNull.Value
        ElseIf IsDBNull(objeto) Then
            Return refencias
        Else
            Return objeto
        End If
    End Function
End Class
